﻿import {
    POST_GROUP, POST_GROUP_SUCCESS, POST_GROUP_ERROR,
    DELETE_GROUP,
    PUT_GROUP,
    GET_GROUPS, GET_GROUPS_SUCCESS, GET_GROUPS_ERROR,
    GET_GROUP, GET_GROUP_SUCCESS, GET_GROUP_ERROR,
    JOIN_GROUP, JOIN_GROUP_SUCCESS, JOIN_GROUP_ERROR, LEAVE_GROUP, LEAVE_GROUP_SUCCESS, LEAVE_GROUP_ERROR,
} from "../Actions/GroupActionTypes";
import * as _ from 'lodash';
import * as update from 'immutability-helper';
import { concatItemsWithId } from "../../Utils/HelperFunctions";
import { GET_POSTS_SUCCESS } from "../Actions/PostActionTypes";

const initialState = {
    groups: [],
    isLoading: false,
    isCreatingNewGroup: false,
    error: null
}

const groupReducer = function (state = initialState, action) {
    switch (action.type) {
        case GET_GROUPS:
            return { ...state, isLoading: true }

        case GET_GROUPS_SUCCESS:
            return update(state, {
                isLoading: { $set: false },
                groups: { $apply: (groups) => concatItemsWithId(groups, action.value) }
            });

        case GET_GROUPS_ERROR:
            return { ...state, isLoading: false, error: action.error }

        case GET_GROUP:
            return { ...state, isLoading: true }

        case GET_GROUP_SUCCESS:
            return update(state, {
                isLoading: { $set: false },
                groups: { $apply: (groups) => concatItemsWithId(groups, [action.value]) }
            });

        case GET_GROUP_ERROR:
            return { ...state, isLoading: false, error: action.error }

        case POST_GROUP:
            return {...state, isCreatingNewGroup: true} ;

        case POST_GROUP_SUCCESS:
            const newGroups = _.cloneDeep(state.groups);
            newGroups.unshift(action.value);
            return { ...state, groups: newGroups, isCreatingNewGroup: false }

        case POST_GROUP_ERROR:
            return { ...state, isCreatingNewGroup: false, error: action.error }

        case JOIN_GROUP:
        case LEAVE_GROUP:
            return update(state, {
                groups: {
                    $apply: (groups) => {
                        return groups.map(g => {
                            if (g.id === action.id) {
                                return update(g, {
                                    isUpdating: { $set: true }
                                });
                            }
                            return g;
                        });
                    }
                }
            });

        case JOIN_GROUP_SUCCESS:
        case LEAVE_GROUP_SUCCESS:
            return update(state, {
                groups: {
                    $apply: (groups) => {
                        return groups.map(g => {
                            if (g.id === action.group.id) {
                                return action.group;
                            }
                            return g;
                        });
                    }
                }
            });

        case JOIN_GROUP_ERROR:
        case LEAVE_GROUP_ERROR:
            return update(state, {
                groups: {
                    $apply: (groups) => {
                        return groups.map(g => {
                            if (g.id === action.id) {
                                return update(g, {
                                    isUpdating: { $set: false },
                                    updateError: { $set: action.error }
                                });
                            }
                            return g;
                        });
                    }
                }
            });

        case GET_POSTS_SUCCESS:
            return update(state, {
                groups: {
                    $apply: (groups) => {
                        const newGroups = action.posts.filter(p => p.targetGroup).map(p => p.targetGroup);
                        return concatItemsWithId(groups, newGroups);
                    }
                }
            });

        case DELETE_GROUP:
            throw Error('deleteGroup is not implemented!');

        case PUT_GROUP:
            throw Error('putGroup is not implemented!');

        default:
            return state;
    }
};

export default groupReducer;