import {
    POST_POST,
    DELETE_POST,
    DELETE_POST_SUCCESS,
    DELETE_POST_ERROR,
    PUT_POST,
    PUT_POST_SUCCESS,
    PUT_POST_ERROR,
    GET_POSTS,
    GET_POSTS_SUCCESS, GET_POSTS_ERROR, POST_POST_SUCCESS, POST_POST_ERROR, GET_REPLIES, GET_REPLIES_SUCCESS, GET_REPLIES_ERROR, GET_POST, GET_POST_SUCCESS, GET_POST_ERROR
} from "../Actions/PostActionTypes";
import * as _ from 'lodash';
import * as update from 'immutability-helper';
import { concatItemsWithId } from "../../Utils/HelperFunctions";
import { GET_GROUP_SUCCESS } from "../Actions/GroupActionTypes";
import { GET_TOPIC_SUCCESS } from "../Actions/TopicActionTypes";
const initialState = {
    posts: [
    ],
    isLoading: false,
    isLoadingPost: false,
    isCreatingNewPost: false,
    error: null
}

const postReducer = function (state = initialState, action) {
    switch (action.type) {
        case GET_POSTS:
            return update(state, {
                isLoading: { $set: true }
            });

        case GET_POSTS_SUCCESS:
            return update(state, {
                isLoading: { $set: false },
                posts: { $set: _.uniqBy(action.posts, p => p.id) }
            });

        case GET_POSTS_ERROR:
            return update(state, {
                isLoading: { $set: false },
                error: { $set: action.error }
            });

        case GET_POST:
            return update(state, {
                isLoadingPost: { $set: true }
            });

        case GET_POST_SUCCESS:
            return update(state, {
                posts: {
                    $apply: (posts) => concatItemsWithId(posts, [action.post])
                },
                isLoadingPost: { $set: false }
            });

        case GET_POST_ERROR:
            return update(state, {
                isLoadingPost: { $set: true },
                error: { $set: action.error }
            });

        case GET_REPLIES:
            return update(state, {
                posts: {
                    $apply: (posts) => {
                        return posts.map(p => {
                            if (p.id === action.id) {
                                return update(p, { isLoadingReplies: { $set: true } });
                            }
                            return p;
                        });
                    }
                }
            });

        case GET_REPLIES_SUCCESS:
            return update(state, {
                posts: {
                    $apply: (posts) => {
                        const oldPosts = posts.map(p => {
                            if (p.id === action.id) {
                                return update(p, { isLoadingReplies: { $set: false }, })
                            }
                            return p;
                        });
                        return concatItemsWithId(oldPosts, action.replies);
                    }
                }
            });

        case GET_REPLIES_ERROR:
            return update(state, {
                posts: {
                    $apply: (posts) => {
                        return posts.map(p => {
                            if (p.id === action.id) {
                                p.isLoadingReplies = false;
                                p.repliesError = action.error;
                            }
                            return p;
                        });
                    }
                }
            });

        case POST_POST:
            return update(state, {
                isCreatingNewPost: { $set: true }
            });

        case POST_POST_SUCCESS:
            return update(state, {
                isCreatingNewPost: { $set: false },
                posts: { $unshift: [action.post] }
            });

        case POST_POST_ERROR:
            return update(state, {
                isCreatingNewPost: { $set: true },
                error: { $set: action.error }
            });

        case DELETE_POST:
            return update(state, {
                posts: {
                    $apply: (posts) => {
                        return posts.map(p => {
                            if (p.id === action.id) {
                                return update(p, {
                                    isDeleting: { $set: true },
                                })
                            }
                            return p;
                        });
                    }
                }
            });

        case DELETE_POST_SUCCESS:
            return update(state, {
                posts: {
                    $apply: (posts) => {
                        return posts.map(p => {
                            if (p.id === action.post.id) {
                                return action.post;
                            }
                            return p;
                        });
                    }
                }
            });

        case DELETE_POST_ERROR:
            return update(state, {
                posts: {
                    $apply: (posts) => {
                        return posts.map(p => {
                            if (p.id === action.id) {
                                return update(p, {
                                    isDeleting: { $set: false },
                                    deleteError: { $set: action.error }
                                })
                            }
                            return p;
                        });
                    }
                }
            });

        case PUT_POST:
            return update(state, {
                posts: {
                    $apply: (posts) => {
                        return posts.map(p => {
                            if (p.id === action.id) {
                                return update(p, {
                                    isUpdating: { $set: true }
                                });
                            }
                            return p;
                        });
                    }
                }
            });

        case PUT_POST_SUCCESS:
            return update(state, {
                posts: {
                    $apply: (posts) => {
                        return posts.map(p => {
                            if (p.id === action.post.id) {
                                return action.post;
                            }
                            return p;
                        });
                    }
                }
            });

        case PUT_POST_ERROR:
            return update(state, {
                posts: {
                    $apply: (posts) => {
                        return posts.map(p => {
                            if (p.id === action.id) {
                                return update(p, {
                                    isUpdating: { $set: false },
                                    updateError: { $set: action.error }
                                });
                            }
                            return p;
                        });
                    }
                }
            });

        case GET_GROUP_SUCCESS:
            return update(state, {
                posts: {
                    $apply: (posts) => concatItemsWithId(posts, action.value.posts)
                }
            })

        case GET_TOPIC_SUCCESS:
            return update(state, {
                posts: {
                    $apply: (posts) => concatItemsWithId(posts, action.value.posts)
                }
            })

        default:
            return state;
    }
};

export default postReducer;