import {
    POST_EVENT,
    DELETE_EVENT,
    PUT_EVENT,
    GET_EVENTS,
    GET_EVENTS_SUCCESS,
    GET_EVENTS_ERROR,
    POST_EVENT_SUCCESS,
    POST_EVENT_ERROR,
    GET_EVENT_SUCCESS,
    GET_EVENT,
    GET_EVENT_ERROR,
    JOIN_EVENT_SUCCESS
} from "../Actions/EventActionTypes";
import * as _ from 'lodash';
import * as update from 'immutability-helper';
import { concatItemsWithId } from "../../Utils/HelperFunctions";

const initialState = {
    events: [
    ],
    isLoading: false,
    isLoadingEvent: false,
    isCreatingNewEvent: false,
    error: null
}

const eventReducer = function (state = initialState, action) {
    switch (action.type) {
        case GET_EVENTS:
            return { ...state, isLoading: true }

        case GET_EVENTS_SUCCESS:
            return { ...state, isLoading: false, events: action.value }

        case GET_EVENTS_ERROR:
            return { ...state, isLoading: false, error: action.error }

        case GET_EVENT:
            return update(state, {
                isLoadingEvent: { $set: true }
            });

        case GET_EVENT_SUCCESS:
            return update(state, {
                isLoadingEvent: { $set: false },
                events: {
                    $apply: events => concatItemsWithId(events, [action.event])
                }
            });

        case GET_EVENT_ERROR:
            return update(state, {
                isLoadingEvent: { $set: false },
                error: { $set: action.error }
            });

        case POST_EVENT:
            return { ...state, isCreatingNewEvent: true }

        case POST_EVENT_SUCCESS:
            const newEvents = _.cloneDeep(state.events);
            newEvents.unshift(action.value);
            return { ...state, events: newEvents, isCreatingNewEvent: false }

        case POST_EVENT_ERROR:
            return { ...state, isCreatingNewEvent: false, error: action.error }

        case DELETE_EVENT:
            throw Error('deleteEvent is not implemented!');

        case PUT_EVENT:
            throw Error('putEvent is not implemented!');

        case JOIN_EVENT_SUCCESS:
            return update(state, {
                events: {
                    $apply: events => {
                        return events.map(e => {
                            if (e.id === action.event.id) {
                                return action.event;
                            }
                            return e;
                        })
                    }
                }
            });

        default:
            return state;
    }
};

export default eventReducer;