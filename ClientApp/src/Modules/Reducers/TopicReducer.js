import {
    POST_TOPIC,
    PUT_TOPIC,
    GET_TOPICS, GET_TOPICS_SUCCESS, GET_TOPICS_ERROR,
    GET_TOPIC, GET_TOPIC_SUCCESS, GET_TOPIC_ERROR,
    JOIN_TOPIC, JOIN_TOPIC_SUCCESS, JOIN_TOPIC_ERROR, POST_TOPIC_SUCCESS, POST_TOPIC_ERROR,
    LEAVE_TOPIC, LEAVE_TOPIC_SUCCESS, LEAVE_TOPIC_ERROR
} from "../Actions/TopicActionTypes";
import { GET_POSTS_SUCCESS } from "../Actions/PostActionTypes";
import * as _ from 'lodash';
import * as update from 'immutability-helper';
import { concatItemsWithId } from "../../Utils/HelperFunctions";

const initialState = {
    topics: [],
    isLoading: false,
    isLoadingTopic: false,
    isCreatingNewTopic: false,
    error: null
}

const topicReducer = function (state = initialState, action) {
    switch (action.type) {
        case GET_TOPICS:
            return { ...state, isLoading: true }

        case GET_TOPICS_SUCCESS:
            return update(state, {
                isLoading: { $set: false },
                topics: { $apply: (topics) => concatItemsWithId(topics, action.value) }
            });

        case GET_TOPICS_ERROR:
            return { ...state, isLoading: false, error: action.error }

        case GET_TOPIC:
            return { ...state, isLoading: true, topicId: action.value }

        case GET_TOPIC_SUCCESS:
            return update(state, {
                isLoading: { $set: false },
                topics: { $apply: (topics) => concatItemsWithId(topics, [action.value]) }
            });

        case GET_TOPIC_ERROR:
            return { ...state, isLoading: false, error: action.error }

        case POST_TOPIC:
            return update(state, {
                isCreatingNewTopic: { $set: true }
            });
        
        case POST_TOPIC_SUCCESS:
            return update(state, {
                isCreatingNewTopic: { $set: false },
                topics: { $unshift: [action.topic] }
            });

        case POST_TOPIC_ERROR:
            return update(state, {
                isCreatingNewTopic: { $set: true },
                error: { $set: action.error }
            });

        case PUT_TOPIC:
            throw Error('putTopic is not implemented!');

        case JOIN_TOPIC:
        case LEAVE_TOPIC:
            return update(state, {
                topics: {
                    $apply: (topics) => {
                        return topics.map(t => {
                            if (t.id === action.id) {
                                return update(t, {
                                    isUpdating: { $set: true }
                                });
                            }
                            return t;
                        });
                    }
                }
            });

        case JOIN_TOPIC_SUCCESS:
        case LEAVE_TOPIC_SUCCESS:
            return update(state, {
                topics: {
                    $apply: (topics) => {
                        return topics.map(t => {
                            if (t.id === action.topic.id) {
                                return action.topic;
                            }
                            return t;
                        });
                    }
                }
            });

        case JOIN_TOPIC_ERROR:
        case LEAVE_TOPIC_ERROR:
            return update(state, {
                topics: {
                    $apply: (topics) => {
                        return topics.map(t => {
                            if (t.id === action.id) {
                                return update(t, {
                                    isUpdating: { $set: false },
                                    updateError: { $set: action.error }
                                });
                            }
                            return t;
                        });
                    }
                }
            });

        case GET_POSTS_SUCCESS:
            return update(state, {
                topics: {
                    $apply: (topics) => {
                        const newTopics = action.posts.filter(p => p.targetTopic).map(p => p.targetTopic);
                        return concatItemsWithId(topics, newTopics);
                    }
                }
            });

        default:
            return state;
    }
};

export default topicReducer;