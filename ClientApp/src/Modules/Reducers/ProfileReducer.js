import { POST_PROFILE, PUT_PROFILE, POST_PROFILE_SUCCESS, 
    POST_PROFILE_ERROR, GET_PROFILE, GET_PROFILE_SUCCESS, 
    GET_PROFILE_ERROR, PUT_PROFILE_SUCCESS, PUT_PROFILE_ERROR, GET_PROFILES, GET_PROFILES_ERROR, GET_PROFILES_SUCCESS
} from "../Actions/ProfileActionTypes";
import { GET_EVENT_SUCCESS } from "../Actions/EventActionTypes";
import * as _ from 'lodash';
import * as update from 'immutability-helper';
import { addManyWithIdToList, concatProfiles } from "../../Utils/HelperFunctions";
import { GET_POSTS_SUCCESS } from "../Actions/PostActionTypes";
import { GET_GROUP_SUCCESS } from "../Actions/GroupActionTypes";
import { GET_TOPIC_SUCCESS } from "../Actions/TopicActionTypes";

const initialState = {
    profiles: [
    ],
    isLoading: false,
    isLoadingProfile: false,
    isCreatingNewProfile: false,
    error: null
}

const profileReducer = function (state = initialState, action) {
    switch (action.type) {
        case GET_PROFILE:
            return update(state, {
                isLoadingProfile: { $set: true }
            });

        case GET_PROFILE_SUCCESS:
            return update(state, {
                profiles: {
                    $apply: (profiles) => concatProfiles(profiles, [action.profile])
                }
            });

        case GET_PROFILE_ERROR: 
            return update(state, {
                isLoadingProfile: { $set: true },
                error: { $set: action.error }
            });

        case POST_PROFILE:
            return update(state, {
                isCreatingNewProfile: { $set: true }
            });

        case POST_PROFILE_SUCCESS:
            return update(state, {
                isCreatingNewProfile: { $set: false },
                profiles: { $unshift: [action.profile] }
            });

        case POST_PROFILE_ERROR:
            return update(state, {
                isCreatingNewProfile: { $set: true },
                error: { $set: action.error }
            });

        case PUT_PROFILE:
            return update(state, {
                profiles: {
                    $apply: (profiles) => {
                        return profiles.map(p => {
                            if (p.id === action.id) {
                                return update(p, {
                                    isUpdating: { $set: true }
                                });
                            }
                            return p;
                        });
                    }
                }
            });

        case PUT_PROFILE_SUCCESS:
            return update(state, {
                profiles: {
                    $apply: (profiles) => concatProfiles(profiles, [action.profile])
                }
            });

        case GET_POSTS_SUCCESS:
            return update(state, {
                profiles: {
                    $apply: (profiles) => {
                        const newProfiles = action.posts.map(p => p.sender);
                        return concatProfiles(profiles, newProfiles);
                    }
                }
            })

        case PUT_PROFILE_ERROR:
            return update(state, {
                profiles: {
                    $apply: (profiles) => {
                        return profiles.map(p => {
                            if (p.id === action.id) {
                                return update(p, {
                                    isUpdating: { $set: false },
                                    updateError: { $set: action.error }
                                });
                            }
                            return p;
                        });
                    }
                }
            });

        case GET_EVENT_SUCCESS:
            return update(state, {
                profiles: {
                    $apply: (profiles) => {
                        const newProfiles = action.event.eventMembers.map(em => em.profile);
                        return concatProfiles(profiles, newProfiles);
                    }
                }
            });

        case GET_PROFILES:
            return update(state, {
                isLoading: { $set: true }
            });

        case GET_PROFILES_SUCCESS:
            return update(state, {
                profiles: {
                    $apply: (profiles) => {
                        const newProfiles = action.profiles;
                        return concatProfiles(profiles, newProfiles);
                    },
                },
                isLoading: { $set: true }
            });

        case GET_PROFILES_ERROR:
            return update(state, {
                error: { $set: action.error },
                isLoading: { $set: false }
            });

        case GET_GROUP_SUCCESS:
            return update(state, {
                profiles: {
                    $apply: (profiles) => {
                        const newPostProfiles = action.value.posts.map(p => p.sender);
                        const newMemberProfiles = action.value.groupMembers.map(gm => gm.profile);
                        const newProfiles = concatProfiles(newPostProfiles, newMemberProfiles);
                        return concatProfiles(profiles, newProfiles);
                    }
                }
            })

        case GET_TOPIC_SUCCESS:
            return update(state, {
                profiles: {
                    $apply: (profiles) => {
                        const newProfiles = action.value.topicMember.map(tm => tm.profile);
                        return concatProfiles(profiles, newProfiles);
                    }
                }
            })

        default:
            return state;
    }
};

export default profileReducer;