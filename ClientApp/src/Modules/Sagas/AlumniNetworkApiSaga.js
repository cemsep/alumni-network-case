import { call, put, takeEvery, takeLatest } from 'redux-saga/effects';
import * as postApi from '../../Utils/PostApiFunctions';
import * as groupApi from '../../Utils/GroupApiFunctions';
import * as topicApi from '../../Utils/TopicApiFunctions';
import * as eventApi from '../../Utils/EventApiFunctions';
import * as profileApi from '../../Utils/ProfileApiFunctions';

import { GET_POSTS, POST_POST, GET_POST, GET_REPLIES, DELETE_POST, PUT_POST } from '../Actions/PostActionTypes';
import { GET_GROUPS, GET_GROUP, POST_GROUP, JOIN_GROUP, LEAVE_GROUP } from '../Actions/GroupActionTypes';
import { GET_TOPICS, GET_TOPIC, POST_TOPIC, JOIN_TOPIC, LEAVE_TOPIC } from '../Actions/TopicActionTypes';
import { GET_EVENTS, POST_EVENT, GET_EVENT, JOIN_EVENT } from '../Actions/EventActionTypes';
import { POST_PROFILE, GET_PROFILE, PUT_PROFILE, GET_PROFILES } from '../Actions/ProfileActionTypes';

import { getPostsSuccessAction, getPostsErrorAction, postPostSuccessAction, postPostErrorAction, getPostSuccessAction, getPostErrorAction, getRepliesSuccessAction, getRepliesErrorAction, deletePostSuccessAction, deletePostErrorAction, putPostSuccessAction, putPostErrorAction } from '../Actions/PostActions';
import { getGroupsSuccessAction, getGroupsErrorAction, getGroupSuccessAction, getGroupErrorAction, postGroupSuccessAction, postGroupErrorAction, joinGroupSuccessAction, joinGroupErrorAction, leaveGroupSuccessAction, leaveGroupErrorAction } from '../Actions/GroupActions';
import { getTopicsSuccessAction, getTopicsErrorAction, getTopicSuccessAction, getTopicErrorAction, postTopicSuccessAction, postTopicErrorAction, joinTopicSuccessAction, joinTopicErrorAction, leaveTopicSuccessAction, leaveTopicErrorAction  } from '../Actions/TopicActions';
import { getEventsSuccessAction, getEventsErrorAction, postEventSuccessAction, postEventErrorAction, getEventSuccessAction, getEventErrorAction, joinEventErrorAction, joinEventSuccessAction } from '../Actions/EventActions';
import { postProfileSuccessAction, postProfileErrorAction, getProfileSuccessAction, getProfileErrorAction, putProfileSuccessAction, putProfileErrorAction, getProfilesSuccessAction, getProfilesErrorAction } from '../Actions/ProfileActions';
import { getUserId } from '../../Utils/HelperFunctions';


function* getPosts(action) {
    try {
        const posts = yield call(postApi.getPosts, action.id, action.endpoint);
        yield put(getPostsSuccessAction(posts));
    } catch (e) {
        yield put(getPostsErrorAction(e));
    }
}

// change to takeLatest if including dynamic filtering/searching
export function* watchGetPosts() {
    yield takeEvery(GET_POSTS, getPosts);
}

function* getPost(action) {
    try {
        const post = yield call(postApi.getPost, action.id);
        yield put(getPostSuccessAction(post));
    } catch (e) {
        yield put(getPostErrorAction(e));
    }
}

export function* watchGetPost() {
    yield takeEvery(GET_POST, getPost);
}

function* postPost(action) {
    try {
        const newPost = yield call(postApi.postPost, action.post);
        yield put(postPostSuccessAction(newPost));
    } catch (e) {
        yield put(postPostErrorAction(e));
    }
}

export function* watchPostPost() {
    yield takeEvery(POST_POST, postPost);
}

function* getReplies(action) {
    try {
        const replies = yield call(postApi.getReplies, action.id);
        yield put(getRepliesSuccessAction(action.id, replies));
    } catch (e) {
        yield put(getRepliesErrorAction(e));
    }
}

export function* watchGetReplies() {
    yield takeEvery(GET_REPLIES, getReplies);
}

function* deletePost(action) {
    try {
        const deletedPost = yield call(postApi.deletePost, action.id);
        yield put(deletePostSuccessAction(deletedPost));
    } catch (e) {
        yield put(deletePostErrorAction(action.id, e));
    }
}

export function* watchDeletePost() {
    yield takeEvery(DELETE_POST, deletePost);
}

function* putPost(action) {
    try {
        const updatedPost = yield call(postApi.putPost, action.id, action.post);
        yield put(putPostSuccessAction(updatedPost));
    } catch (e) {
        yield put(putPostErrorAction(e));
    }
}

export function* watchPutPost() {
    yield takeEvery(PUT_POST, putPost);
}

function* getGroups(action) {
    try {
        const groups = yield call(groupApi.getGroups);
        yield put(getGroupsSuccessAction(groups));
    } catch (e) {
        yield put(getGroupsErrorAction(e));
    }
}

// change to takeLatest if including dynamic filtering/searching
export function* watchGetGroups() {
    yield takeEvery(GET_GROUPS, getGroups);
}

function* getGroup(action) {
    try {
        const group = yield call(groupApi.getGroup, action.value);
        yield put(getGroupSuccessAction(group));
    } catch (e) {
        yield put(getGroupErrorAction(e));
    }
}

// change to takeLatest if including dynamic filtering/searching
export function* watchGetGroup() {
    yield takeEvery(GET_GROUP, getGroup);
}

function* postGroup(action) {
    try {
        const newGroup = yield call(groupApi.postGroup, action.value);
        yield put(postGroupSuccessAction(newGroup));
    } catch (e) {
        yield put(postGroupErrorAction(e));
    }
}

export function* watchGroupPost() {
    yield takeEvery(POST_GROUP, postGroup);
}

function* joinGroup(action) {
    try {
        const updatedGroup = yield call(groupApi.joinGroup, action.id, action.userIds);
        yield put(joinGroupSuccessAction(updatedGroup));
    } catch (e) {
        yield put(joinGroupErrorAction(e));
    }
}

export function* watchjoinGroup() {
    yield takeEvery(JOIN_GROUP, joinGroup);
}


function* leaveGroup(action) {
    try {
        const updatedGroup = yield call(groupApi.leaveGroup, action.id);
        yield put(leaveGroupSuccessAction(updatedGroup));
    } catch (e) {
        yield put(leaveGroupErrorAction(e));
    }
}

export function* watchLeaveGroup() {
    yield takeEvery(LEAVE_GROUP, leaveGroup);
}


function* getTopics(action) {
    try {
        const topics = yield call(topicApi.getTopics);
        yield put(getTopicsSuccessAction(topics));
    } catch (e) {
        yield put(getTopicsErrorAction(e));
    }
}

// change to takeLatest if including dynamic filtering/searching
export function* watchGetTopics() {
    yield takeEvery(GET_TOPICS, getTopics);
}

function* getTopic(action) {
    try {
        const topic = yield call(topicApi.getTopic, action.value);
        yield put(getTopicSuccessAction(topic));
    } catch (e) {
        yield put(getTopicErrorAction(e));
    }
}

// change to takeLatest if including dynamic filtering/searching
export function* watchGetTopic() {
    yield takeEvery(GET_TOPIC, getTopic);
}

function* postTopic(action) {
    try {
        const newTopic = yield call(topicApi.postTopic, action.topic);
        yield put(postTopicSuccessAction(newTopic));
    } catch (e) {
        yield put(postTopicErrorAction(e));
    }
}

export function* watchPostTopic() {
    yield takeEvery(POST_TOPIC, postTopic);
}

function* joinTopic(action) {
    try {
        const updatedTopic = yield call(topicApi.joinTopic, action.id);
        yield put(joinTopicSuccessAction(updatedTopic));
    } catch (e) {
        yield put(joinTopicErrorAction(e));
    }
}

export function* watchjoinTopic() {
    yield takeEvery(JOIN_TOPIC, joinTopic);
}

function* leaveTopic(action) {
    try {
        const updatedTopic = yield call(topicApi.leaveTopic, action.id);
        yield put(leaveTopicSuccessAction(updatedTopic));
    } catch (e) {
        yield put(leaveTopicErrorAction(e));
    }
}

export function* watchLeaveTopic() {
    yield takeEvery(LEAVE_TOPIC, leaveTopic);
}

function* getEvents(action) {
    try {
        const events = yield call(eventApi.getEvents);
        yield put(getEventsSuccessAction(events));
    } catch (e) {
        yield put(getEventsErrorAction(e));
    }
}

// change to takeLatest if including dynamic filtering/searching
export function* watchGetEvents() {
    yield takeEvery(GET_EVENTS, getEvents);
}

function* getEvent(action) {
    try {
        const event = yield call(eventApi.getEvent, action.id);
        yield put(getEventSuccessAction(event));
    } catch (e) {
        yield put(getEventErrorAction(e));
    }
}

export function* watchGetEvent() {
    yield takeLatest(GET_EVENT, getEvent);
}

function* postEvent(action) {
    try {
        const newEvent = yield call(eventApi.postEvent, action.value);
        yield put(postEventSuccessAction(newEvent));
    } catch (e) {
        yield put(postEventErrorAction(e));
    }
}

export function* watchPostEvent() {
    yield takeEvery(POST_EVENT, postEvent);
}


function* getProfile(action) {
    try {
        const profile = yield call(profileApi.getProfile, action.id);
        yield put(getProfileSuccessAction(profile));
    } catch (e) {
        yield put(getProfileErrorAction(e));
    }
}

export function* watchGetProfile() {
    yield takeEvery(GET_PROFILE, getProfile);
}

function* postProfile(action) {
    try {
        const newProfile = yield call(profileApi.postProfile, action.profile);
        yield put(postProfileSuccessAction(newProfile));
    } catch (e) {
        yield put(postProfileErrorAction(e));
    }
}

export function* watchPostProfile() {
    yield takeEvery(POST_PROFILE, postProfile);
}

function* putProfile(action) {
    try {
        const updatedProfile = yield call(profileApi.putProfile, action.id, action.profile);
        yield put(putProfileSuccessAction(updatedProfile));
    } catch (e) {
        yield put(putProfileErrorAction(e));
    }
}

export function* watchPutProfile() {
    yield takeEvery(PUT_PROFILE, putProfile);
}

function* joinEvent(action) {
    try {
        const updatedEvent = yield call(eventApi.joinEvent, action.eventId, action.userIds);
        yield put(joinEventSuccessAction(updatedEvent));
    } catch (e) {
        yield put(joinEventErrorAction(e));
    }
}

export function* watchJoinEvent() {
    yield takeEvery(JOIN_EVENT, joinEvent);
}

function* getProfiles(action) {
    try {
        const profiles = yield call(profileApi.getProfiles);
        yield put(getProfilesSuccessAction(profiles));
    } catch (e) {
        yield put(getProfilesErrorAction(e));
    }
}

export function* watchGetProfiles() {
    yield takeLatest(GET_PROFILES, getProfiles);
}
