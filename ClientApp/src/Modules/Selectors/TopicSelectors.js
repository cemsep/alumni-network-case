
export const topicsSelector = ({ topicState, authState }) => {
  return {
      topics: topicState.topics,
      isLoading: topicState.isLoading,
      error: topicState.error,
      userId: authState.userId
  };
}

export const topicSelector = ({ topicState, authState }, props) => {
    const topic = topicState.topics.find(t => t.id === props.id);
    const userId = authState.userId;
    const isMember = topic && topic.topicMember && topic.topicMember.find(tm => tm.profileId === userId);
    return {
        topic,
        userId,
        isMember
    };  
}

export const topicEventsSelector = ({ topicState }, props) => {
    const topic = topicState.topics.find(t => t.id === props.id);
    var events = [];
    topic && topic.eventTopicInvite && topic.eventTopicInvite.map(eti => events.push(eti.event));
    return {
        events
    }
}