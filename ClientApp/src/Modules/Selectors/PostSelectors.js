﻿
export const postsSelector = ({ postState }) => {
    return {
        posts: postState.posts.filter(p => !p.replyParentId && !p.deleted),
        isLoading: postState.isLoading,
        error: postState.error
    };
}

export const postSelector = ({ postState }, props) => {
    const post = postState.posts.find(post => post.id === props.id);
    return { post };
}

export const threadSelector = ({ postState }, props) => {
    const post = postState.posts.find(p => p.id === props.id)
    const replies = postState.posts.filter(p => p.replyParentId === props.id);
    return { post, replies };
}

export const repliesSelector = ({ postState, profileState }, props) => {
    const replies = postState.posts.filter(p => p.replyParentId === props.post.id).reverse();
    const sender = profileState.profiles.find(p => p.userId === props.post.senderId);
    return { replies, sender };
}

export const groupPostsSelector = ({ postState }, { id }) => {
    return {
        posts: postState.posts.filter(p => (p.targetGroupId === id) && !p.replyParentId && !p.deleted),
        isLoading: postState.isLoading,
        error: postState.error
    }
}

export const topicPostsSelector = ({ postState }, { id }) => {
    return {
        posts: postState.posts.filter(p => (p.targetTopicId === id) && !p.replyParentId && !p.deleted),
        isLoading: postState.isLoading,
        error: postState.error
    }
}

export const postDetailsSelector = ({ profileState, topicState, groupState }, props) => {
    const sender = profileState.profiles.find(profile => profile.userId === props.post.senderId);
    const targetUser = profileState.profiles.find(profile => profile.userId === props.post.targetUserId);
    const targetTopic = topicState.topics.find(topic => topic.id === props.post.targetTopicId);
    const targetGroup = groupState.groups.find(group => group.id === props.post.targetGroupId);
    return {
        sender,
        targetUser,
        targetTopic,
        targetGroup
    }
}