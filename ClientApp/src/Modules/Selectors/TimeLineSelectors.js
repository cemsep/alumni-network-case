﻿import { sortPostsAndEvents } from "../../Utils/HelperFunctions";

export const timeLineSelector = ({ postState, eventState }) => {
    const posts = postState.posts.filter(p => !p.replyParentId && !p.deleted);
    const events = eventState.events;
    const postsAndEvents = posts.concat(events);
    postsAndEvents.sort(sortPostsAndEvents);
    return {
        postsAndEvents,
        isLoading: postState.isLoading || eventState.isLoading,
        error: postState.error || eventState.error
    };
}
