﻿
export const groupsSelector = ({ groupState, authState }) => {
    return {
        groups: groupState.groups,
        isLoading: groupState.isLoading,
        error: groupState.error,
        userId: authState.userId
    };
}

export const groupSelector = ({ groupState, authState }, props) => {
    const group = groupState.groups.find(g => g.id === props.id);
    const userId = authState.userId;
    const isMember = group && group.members && group.members.find(g => g === userId);
    return {
        group,
        userId,
        isMember
    };
}