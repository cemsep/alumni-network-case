import moment from 'moment';

export const eventsSelector = ({ eventState }) => {
  return {
      events: eventState.events,
      isLoading: eventState.isLoading,
      error: eventState.error
  };
}

export const eventSelector = ({ eventState, authState }, props) => {
    const event = eventState.events.find(event => event.id === props.id);
    const isMember = !!(event && event.members && event.members.find(m => m === authState.userId));
    return { event, isMember, userId: authState.userId, isLoading: eventState.isLoadingEvent };
}

export const calendarSelector = ({ eventState, authState }) => {
    const events = eventState.events.map(event => {
        const isMember = !!(event && event.members && event.members.find(m => m === authState.userId));
        return { start: moment.utc(event.startTime).toDate(), end: moment.utc(event.endTime).toDate(), title: event.name, id: event.id, isMember };
    });
    return {
        events
    };
}