
export const profileSelector = (state, props) => {
    return { profile: state.profileState.profiles.find(profile => profile.userId === props.id), userId: state.authState.userId, error: state.profileState.error };
}

export const profilesSelector = ({ profileState }) => {
    return { profiles: profileState.profiles  }
}

export const profileSelectSelector = ({ profileState, authState }, props) => {
    const userId = authState.userId;
    const profiles = props.filter ? profileState.profiles.filter(p => !props.filter.find(f => p.userId === f) && p.userId !== userId) : profileState.profiles.filter(p => p.userId !== userId);
    return { profiles };
}