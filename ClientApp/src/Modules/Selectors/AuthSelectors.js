﻿export const authSelector = ({ authState }) => {
    return {
        isAuthenticated: authState.isAuthenticated,
        userId: authState.userId
    };
}

export const isAuthorOfPostSelector = ({ authState }, props) => {
    return {
        isAuthor: authState.userId === (props.post && props.post.senderId)
    }
}