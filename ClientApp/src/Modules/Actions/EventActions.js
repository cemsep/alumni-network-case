import { GET_EVENTS, POST_EVENT, DELETE_EVENT, PUT_EVENT, GET_EVENTS_SUCCESS, GET_EVENTS_ERROR, POST_EVENT_SUCCESS, POST_EVENT_ERROR, GET_EVENT, GET_EVENT_SUCCESS, GET_EVENT_ERROR, JOIN_EVENT, JOIN_EVENT_SUCCESS, JOIN_EVENT_ERROR } from "./EventActionTypes";

export const getEventsAction = () => ({
    type: GET_EVENTS
});

export const getEventsSuccessAction = (events) => ({
    type: GET_EVENTS_SUCCESS,
    value: events
});

export const getEventsErrorAction = (error) => ({
    type: GET_EVENTS_ERROR,
    error
});

export const getEventAction = (id) => ({
    type: GET_EVENT,
    id
});

export const getEventSuccessAction = (event) => ({
    type: GET_EVENT_SUCCESS,
    event
});

export const getEventErrorAction = (error) => ({
    type: GET_EVENT_ERROR,
    error
});

export const postEventAction = event => ({
    type: POST_EVENT,
    value: event
});

export const postEventSuccessAction = (event) => ({
    type: POST_EVENT_SUCCESS,
    value: event
});

export const postEventErrorAction = (error) => ({
    type: POST_EVENT_ERROR,
    error
});

export const deleteEventAction = id => ({
    type: DELETE_EVENT,
    value: id
});

export const putEventAction = (id, event) => ({
    type: PUT_EVENT,
    value: { id, event }
});

export const joinEventAction = (eventId, userIds) => ({
    type: JOIN_EVENT,
    eventId,
    userIds
});

export const joinEventSuccessAction = (event) => ({
    type: JOIN_EVENT_SUCCESS,
    event
});

export const joinEventErrorAction = (error) => ({
    type: JOIN_EVENT_ERROR,
    error
});