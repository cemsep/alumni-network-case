﻿import { bindActionCreators } from 'redux';
import {
    getPostsAction,
    getPostsSuccessAction,
    getPostsErrorAction,
    getPostAction,
    getPostSuccessAction,
    getPostErrorAction,
    postPostAction,
    deletePostAction,
    deletePostSuccessAction,
    deletePostErrorAction,
    putPostAction,
    putPostSuccessAction,
    putPostErrorAction,
    postPostSuccessAction,
    postPostErrorAction,
    getRepliesAction,
    getRepliesSuccessAction,
    getRepliesErrorAction
} from './PostActions';
import store from '../Store/Store';


const actions = {
    getPostsAction,
    getPostsSuccessAction,
    getPostsErrorAction,
    getRepliesAction,
    getRepliesSuccessAction,
    getRepliesErrorAction,
    getPostAction,
    getPostSuccessAction,
    getPostErrorAction,
    postPostAction,
    postPostSuccessAction,
    postPostErrorAction,
    deletePostAction,
    deletePostSuccessAction,
    deletePostErrorAction,
    putPostAction,
    putPostSuccessAction,
    putPostErrorAction
}

const postActionDispatchers = bindActionCreators(actions, store.dispatch);

export default postActionDispatchers;