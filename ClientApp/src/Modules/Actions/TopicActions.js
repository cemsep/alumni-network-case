import {
    GET_TOPICS, GET_TOPICS_SUCCESS, GET_TOPICS_ERROR,
    GET_TOPIC, GET_TOPIC_SUCCESS, GET_TOPIC_ERROR,
    POST_TOPIC, POST_TOPIC_SUCCESS, POST_TOPIC_ERROR,
    PUT_TOPIC,
    JOIN_TOPIC, JOIN_TOPIC_SUCCESS, JOIN_TOPIC_ERROR,
    LEAVE_TOPIC, LEAVE_TOPIC_SUCCESS, LEAVE_TOPIC_ERROR
} from "./TopicActionTypes";

export const getTopicsAction = () => ({
    type: GET_TOPICS
});

export const getTopicsSuccessAction = (topics) => ({
    type: GET_TOPICS_SUCCESS,
    value: topics
});

export const getTopicsErrorAction = (error) => ({
    type: GET_TOPICS_ERROR,
    error
});



export const getTopicAction = (topicId) => ({
    type: GET_TOPIC,
    value: topicId
});

export const getTopicSuccessAction = (topic) => ({
    type: GET_TOPIC_SUCCESS,
    value: topic
});

export const getTopicErrorAction = (error) => ({
    type: GET_TOPIC_ERROR,
    error
});



export const postTopicAction = (topic) => ({
    type: POST_TOPIC,
    topic
});

export const postTopicSuccessAction = (topic) => ({
    type: POST_TOPIC_SUCCESS,
    topic
});

export const postTopicErrorAction = (error) => ({
    type: POST_TOPIC_ERROR,
    error
});

export const putTopicAction = (id, topic) => ({
    type: PUT_TOPIC,
    value: { id, topic }
});

export const joinTopicAction = (id) => ({
    type: JOIN_TOPIC,
    id
});

export const joinTopicSuccessAction = (topic) => ({
    type: JOIN_TOPIC_SUCCESS,
    topic
});

export const joinTopicErrorAction = (id, error) => ({
    type: JOIN_TOPIC_ERROR,
    id,
    error
});

export const leaveTopicAction = (id) => ({
    type: LEAVE_TOPIC,
    id
});

export const leaveTopicSuccessAction = (topic) => ({
    type: LEAVE_TOPIC_SUCCESS,
    topic
});

export const leaveTopicErrorAction = (id, error) => ({
    type: LEAVE_TOPIC_ERROR,
    id,
    error
});