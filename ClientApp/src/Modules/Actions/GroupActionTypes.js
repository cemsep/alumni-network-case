﻿export const GET_GROUPS = 'GET_GROUPS';
export const GET_GROUPS_SUCCESS = 'GET_GROUPS_SUCCESS';
export const GET_GROUPS_ERROR = 'GET_GROUPS_ERROR';

export const GET_GROUP = 'GET_GROUP';
export const GET_GROUP_SUCCESS = 'GET_GROUP_SUCCESS';
export const GET_GROUP_ERROR = 'GET_GROUP_ERROR';

export const POST_GROUP = 'POST_GROUP';
export const POST_GROUP_SUCCESS = 'POST_GROUP_SUCCESS';
export const POST_GROUP_ERROR = 'POST_GROUP_ERROR';

export const JOIN_GROUP = 'JOIN_GROUP';
export const JOIN_GROUP_SUCCESS = 'JOIN_GROUP_SUCCESS';
export const JOIN_GROUP_ERROR = 'JOIN_GROUP_ERROR';

export const LEAVE_GROUP = 'LEAVE_GROUP';
export const LEAVE_GROUP_SUCCESS = 'LEAVE_GROUP_SUCCESS';
export const LEAVE_GROUP_ERROR = 'LEAVE_GROUP_ERROR';

export const DELETE_GROUP = 'DELETE_GROUP';
export const PUT_GROUP = 'PUT_GROUP';