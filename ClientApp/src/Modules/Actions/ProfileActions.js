import { POST_PROFILE, PUT_PROFILE, POST_PROFILE_SUCCESS, 
    POST_PROFILE_ERROR, GET_PROFILE, GET_PROFILE_SUCCESS, 
    GET_PROFILE_ERROR, PUT_PROFILE_SUCCESS, PUT_PROFILE_ERROR, GET_PROFILES, GET_PROFILES_SUCCESS, GET_PROFILES_ERROR } from "./ProfileActionTypes";

export const getProfileAction = (id) => ({
    type: GET_PROFILE,
    id
});

export const getProfileSuccessAction = (profile) => ({
    type: GET_PROFILE_SUCCESS,
    profile
});

export const getProfileErrorAction = (error) => ({
    type: GET_PROFILE_ERROR,
    error
});

export const postProfileAction = (profile) => ({
    type: POST_PROFILE,
    profile
});

export const postProfileSuccessAction = (profile) => ({
    type: POST_PROFILE_SUCCESS,
    profile
});

export const postProfileErrorAction = (error) => ({
    type: POST_PROFILE_ERROR,
    error
});

export const putProfileAction = (id, profile) => ({
    type: PUT_PROFILE,
    id,
    profile
});

export const putProfileSuccessAction = (profile) => ({
    type: PUT_PROFILE_SUCCESS,
    profile
});

export const putProfileErrorAction = (id, error) => ({
    type: PUT_PROFILE_ERROR,
    id,
    error
});

export const getProfilesAction = () => ({
    type: GET_PROFILES
});

export const getProfilesSuccessAction = (profiles) => ({
    type: GET_PROFILES_SUCCESS,
    profiles
});

export const getProfilesErrorAction = (error) => ({
    type: GET_PROFILES_ERROR,
    error
});