﻿import { bindActionCreators, ActionCreatorsMapObject } from 'redux';
import {
    getGroupsAction, getGroupsSuccessAction, getGroupsErrorAction,
    getGroupAction, getGroupSuccessAction, getGroupErrorAction,
    postGroupAction, postGroupSuccessAction, postGroupErrorAction,
    joinGroupAction, joinGroupSuccessAction, joinGroupErrorAction,
    leaveGroupAction, leaveGroupSuccessAction, leaveGroupErrorAction,
    deleteGroupAction,
    putGroupAction, 
} from './GroupActions';
import store from '../Store/Store';


const actions = {
    getGroupsAction,
    getGroupsSuccessAction,
    getGroupsErrorAction,
    getGroupAction,
    getGroupSuccessAction,
    getGroupErrorAction,
    postGroupAction,
    postGroupSuccessAction,
    postGroupErrorAction,
    joinGroupAction,
    joinGroupSuccessAction,
    joinGroupErrorAction,
    leaveGroupAction,
    leaveGroupSuccessAction,
    leaveGroupErrorAction,
    deleteGroupAction,
    putGroupAction
}

const groupActionDispatchers = bindActionCreators(actions, store.dispatch);

export default groupActionDispatchers;