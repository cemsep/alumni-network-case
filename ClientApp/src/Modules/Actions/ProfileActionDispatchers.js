import { bindActionCreators } from 'redux';
import {
    getProfileAction,
    getProfileSuccessAction,
    getProfileErrorAction,
    postProfileAction,
    putProfileAction,
    postProfileSuccessAction,
    postProfileErrorAction,
    putProfileSuccessAction,
    putProfileErrorAction,
    getProfilesAction,
    getProfilesErrorAction,
    getProfilesSuccessAction
} from './ProfileActions';
import store from '../Store/Store';


const actions = {
    getProfileAction,
    getProfileSuccessAction,
    getProfileErrorAction,
    postProfileAction,
    putProfileAction,
    postProfileSuccessAction,
    postProfileErrorAction,
    putProfileSuccessAction,
    putProfileErrorAction,
    getProfilesAction,
    getProfilesErrorAction,
    getProfilesSuccessAction
}

const profileActionDispatchers = bindActionCreators(actions, store.dispatch);

export default profileActionDispatchers;