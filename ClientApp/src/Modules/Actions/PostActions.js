﻿import {
    GET_POSTS,
    POST_POST,
    GET_POSTS_SUCCESS,
    GET_POSTS_ERROR,
    POST_POST_SUCCESS,
    POST_POST_ERROR,
    GET_REPLIES,
    GET_REPLIES_SUCCESS,
    GET_REPLIES_ERROR,
    GET_POST,
    GET_POST_SUCCESS,
    GET_POST_ERROR,
    DELETE_POST,
    DELETE_POST_SUCCESS,
    DELETE_POST_ERROR,
    PUT_POST,
    PUT_POST_SUCCESS,
    PUT_POST_ERROR
} from "./PostActionTypes";

export const getPostsAction = (id = null, endpoint = 'api/post') => ({
    type: GET_POSTS,
    id,
    endpoint
});

export const getPostsSuccessAction = (posts) => ({
    type: GET_POSTS_SUCCESS,
    posts
});

export const getPostsErrorAction = (error) => ({
    type: GET_POSTS_ERROR,
    error
});

export const getPostAction = (id) => ({
    type: GET_POST,
    id
});

export const getPostSuccessAction = (post) => ({
    type: GET_POST_SUCCESS,
    post
});

export const getPostErrorAction = (error) => ({
    type: GET_POST_ERROR,
    error
});

export const getRepliesAction = (id) => ({
    type: GET_REPLIES,
    id
});

export const getRepliesSuccessAction = (id, replies) => ({
    type: GET_REPLIES_SUCCESS,
    id,
    replies
});

export const getRepliesErrorAction = (id, error) => ({
    type: GET_REPLIES_ERROR,
    id,
    error
});

export const postPostAction = (post) => ({
    type: POST_POST,
    post
});

export const postPostSuccessAction = (post) => ({
    type: POST_POST_SUCCESS,
    post
});

export const postPostErrorAction = (error) => ({
    type: POST_POST_ERROR,
    error
});

export const deletePostAction = (id) => ({
    type: DELETE_POST,
    id
});

export const deletePostSuccessAction = (post) => ({
    type: DELETE_POST_SUCCESS,
    post
});

export const deletePostErrorAction = (id, error) => ({
    type: DELETE_POST_ERROR,
    id,
    error
});

export const putPostAction = (id, post) => ({
    type: PUT_POST,
    id,
    post
});

export const putPostSuccessAction = (post) => ({
    type: PUT_POST_SUCCESS,
    post
});

export const putPostErrorAction = (id, error) => ({
    type: PUT_POST_ERROR,
    id,
    error
});
