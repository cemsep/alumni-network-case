﻿import {
    GET_GROUPS, GET_GROUPS_SUCCESS, GET_GROUPS_ERROR,
    POST_GROUP, POST_GROUP_SUCCESS, POST_GROUP_ERROR,
    JOIN_GROUP, JOIN_GROUP_SUCCESS, JOIN_GROUP_ERROR,
    DELETE_GROUP,
    PUT_GROUP,
    GET_GROUP, GET_GROUP_SUCCESS, GET_GROUP_ERROR,
    LEAVE_GROUP, LEAVE_GROUP_SUCCESS, LEAVE_GROUP_ERROR
} from "./GroupActionTypes";

export const getGroupsAction = () => ({
    type: GET_GROUPS
});

export const getGroupsSuccessAction = (groups) => ({
    type: GET_GROUPS_SUCCESS,
    value: groups
});

export const getGroupsErrorAction = (error) => ({
    type: GET_GROUPS_ERROR,
    error
});



export const getGroupAction = (groupId) => ({
    type: GET_GROUP,
    value: groupId
});

export const getGroupSuccessAction = (group) => ({
    type: GET_GROUP_SUCCESS,
    value: group
});

export const getGroupErrorAction = (error) => ({
    type: GET_GROUP_ERROR,
    error
});



export const postGroupAction = group => ({
    type: POST_GROUP,
    value: group
});

export const postGroupSuccessAction = (group) => ({
    type: POST_GROUP_SUCCESS,
    value: group
});

export const postGroupErrorAction = (error) => ({
    type: POST_GROUP_ERROR,
    error
});



export const joinGroupAction = (id, userIds) => ({
    type: JOIN_GROUP,
    id,
    userIds
});

export const joinGroupSuccessAction = (group) => ({
    type: JOIN_GROUP_SUCCESS,
    group
});

export const joinGroupErrorAction = (id, error) => ({
    type: JOIN_GROUP_ERROR,
    id,
    error
});


export const leaveGroupAction = (id) => ({
    type: LEAVE_GROUP,
    id
});

export const leaveGroupSuccessAction = (group) => ({
    type: LEAVE_GROUP_SUCCESS,
    group
});

export const leaveGroupErrorAction = (id, error) => ({
    type: LEAVE_GROUP_ERROR,
    id,
    error
});



export const deleteGroupAction = id => ({
    type: DELETE_GROUP,
    value: id
});

export const putGroupAction = (id, group) => ({
    type: PUT_GROUP,
    value: { id, group }
});