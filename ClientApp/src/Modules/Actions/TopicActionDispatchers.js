import { bindActionCreators, ActionCreatorsMapObject } from 'redux';
import {
    getTopicsAction, getTopicsSuccessAction, getTopicsErrorAction,
    getTopicAction, getTopicSuccessAction, getTopicErrorAction,
    postTopicAction, postTopicSuccessAction, postTopicErrorAction,
    putTopicAction,
    joinTopicAction, joinTopicSuccessAction, joinTopicErrorAction,
    leaveTopicAction, leaveTopicSuccessAction, leaveTopicErrorAction
} from './TopicActions';
import store from '../Store/Store';


const actions = {
    getTopicsAction,
    getTopicsSuccessAction,
    getTopicsErrorAction,
    getTopicAction,
    getTopicSuccessAction,
    getTopicErrorAction,
    postTopicAction,
    postTopicSuccessAction,
    postTopicErrorAction,
    putTopicAction,
    joinTopicAction,
    joinTopicSuccessAction,
    joinTopicErrorAction,
    leaveTopicAction,
    leaveTopicSuccessAction,
    leaveTopicErrorAction
}

const topicActionDispatchers = bindActionCreators(actions, store.dispatch);

export default topicActionDispatchers;