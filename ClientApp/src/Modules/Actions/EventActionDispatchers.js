import { bindActionCreators } from 'redux';
import {
    getEventsAction,
    getEventsSuccessAction,
    getEventsErrorAction,
    postEventAction,
    deleteEventAction,
    putEventAction,
    postEventSuccessAction,
    postEventErrorAction,
    getEventAction,
    getEventSuccessAction,
    getEventErrorAction,
    joinEventAction,
    joinEventErrorAction,
    joinEventSuccessAction
} from './EventActions';
import store from '../Store/Store';


const actions = {
    getEventsAction,
    getEventsSuccessAction,
    getEventsErrorAction,
    getEventAction,
    getEventSuccessAction,
    getEventErrorAction,
    postEventAction,
    postEventSuccessAction,
    postEventErrorAction,
    deleteEventAction,
    putEventAction,
    joinEventAction,
    joinEventErrorAction,
    joinEventSuccessAction
}

const eventActionDispatchers = bindActionCreators(actions, store.dispatch);

export default eventActionDispatchers;