﻿import { createStore, applyMiddleware, combineReducers } from 'redux';
import createSagaMiddleware from 'redux-saga';
import postReducer from '../Reducers/PostReducer';
import root from './Index';
import groupReducer from '../Reducers/GroupReducer';
import topicReducer from '../Reducers/TopicReducer';
import eventReducer from '../Reducers/EventReducer';
import authReducer from '../Reducers/AuthReducer';
import profileReducer from '../Reducers/ProfileReducer';

const sagaMiddleware = createSagaMiddleware();

const reducers = combineReducers({
    authState: authReducer,
    postState: postReducer, 
    groupState: groupReducer, 
    topicState: topicReducer,
    eventState: eventReducer,
    profileState: profileReducer
  });

const store = createStore(reducers, applyMiddleware(sagaMiddleware));

sagaMiddleware.run(root)
export default store;