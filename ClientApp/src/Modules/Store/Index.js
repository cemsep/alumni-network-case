﻿import { fork } from 'redux-saga/effects';
import {
    watchGetPosts,
	watchGetGroups,
    watchGetGroup,
    watchjoinGroup,
    watchLeaveGroup,
    watchGetTopics,
    watchGetTopic,
    watchjoinTopic,
    watchLeaveTopic,
	watchPostPost,
	watchGroupPost,
	watchPostTopic,
	watchGetEvents,
	watchPostEvent,
	watchGetPost,
    watchGetReplies,
    watchPutPost,
    watchDeletePost,
    watchGetProfile,
    watchPostProfile,
    watchPutProfile,
    watchGetEvent,
    watchJoinEvent,
    watchGetProfiles
} from '../Sagas/AlumniNetworkApiSaga';

function* root() {
    yield fork(watchGetPosts);
    yield fork(watchPostPost);
    yield fork(watchGetGroups);
    yield fork(watchGetGroup);
    yield fork(watchjoinGroup);
    yield fork(watchLeaveGroup);
    yield fork(watchGroupPost);
    yield fork(watchGetTopics);
    yield fork(watchGetTopic);
    yield fork(watchjoinTopic);
    yield fork(watchLeaveTopic);
    yield fork(watchPostTopic);
    yield fork(watchGetEvents);
    yield fork(watchPostEvent);
    yield fork(watchGetPost);
    yield fork(watchGetReplies);
    yield fork(watchPutPost);
    yield fork(watchDeletePost);
    yield fork(watchGetProfile);
    yield fork(watchPostProfile);
    yield fork(watchPutProfile);
    yield fork(watchGetEvent);
    yield fork(watchJoinEvent);
    yield fork(watchGetProfiles);
}

export default root;