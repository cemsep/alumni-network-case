import * as axios from 'axios';
import authService from '../components/api-authorization/AuthorizeService';

export async function getEvents() {
    const token = await authService.getAccessToken();
    const response = await axios.get('api/event', {
        headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    return response.data.map(event => {
        return { ...event, members: event.eventMembers && event.eventMembers.map(m => m.profileId)}
    });
}

export async function getEvent(id) {
    const token = await authService.getAccessToken();
    const response = await axios.get(`api/event/${id}`, {
        headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    return { ...response.data, members: response.data.eventMembers && response.data.eventMembers.map(m => m.profileId) }
}

export async function postEvent(event) {
    const token = await authService.getAccessToken();
    const response = await axios.post('api/event', event, {
        headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    return { ...response.data, members: response.data.eventMembers && response.data.eventMembers.map(m => m.profileId) };
}

export async function joinEvent(eventId, userIds) {
    const token = await authService.getAccessToken();
    let response = await axios.post(`api/event/${eventId}/join`, userIds || [], {
        headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    return { ...response.data, members: response.data.eventMembers && response.data.eventMembers.map(m => m.profileId) };
}