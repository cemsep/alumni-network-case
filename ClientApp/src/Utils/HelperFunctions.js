﻿import * as _ from 'lodash';
import authService from '../components/api-authorization/AuthorizeService';

// helper functions for reducers

export function concatItemsWithId(oldItems, newItems) {
    const newList = newItems.concat(oldItems);
    return _.uniqBy(newList, i => i.id);
}

export function concatProfiles(oldProfiles, newProfiles) {
    const newList = [].concat(newProfiles).concat(oldProfiles);
    return _.uniqBy(newList, i => i.userId);
}

export function stringifyDateTime(time) {
    const dateTime = new Date(time).toDateString().split(' ');
    const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    const day = days[new Date(time).getDay()]
    const clock = new Date(time).toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' });
    return `${day}, ${dateTime[2]} ${dateTime[1]} ${dateTime[3]} at ${clock }`;
}

export function sortPostsAndEvents(a, b) {
    const aValue = a.createdOn || a.lastUpdated;
    const bValue = b.createdOn || b.lastUpdated;

    let comparison = 0;
    if (aValue > bValue) {
        comparison = -1;
    } else if (aValue < bValue) {
        comparison = 1;
    }
    return comparison;
}

export async function getUserId() {
    return (await authService.getUser()).sub;
}