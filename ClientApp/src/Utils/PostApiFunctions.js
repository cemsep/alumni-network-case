import * as axios from 'axios';
import authService from '../components/api-authorization/AuthorizeService';

export async function getPosts(id, endpoint) {
    const token = await authService.getAccessToken();
    if (id !== null)
        endpoint = `${endpoint}/${id}`;
    const response = await axios.get(endpoint, {
        headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    return response.data;
}

export async function getPost(id) {
    const token = await authService.getAccessToken();
    const response = await axios.get(`api/post/${id}`, {
        headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    return response.data;
}

export async function getReplies(replyParentId) {
    const token = await authService.getAccessToken();
    const response = await axios.get(`api/post?replyParentId=${replyParentId}`, {
        headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    return response.data;
}

export async function postPost(post) {
    const token = await authService.getAccessToken();
    const response = await axios.post('api/post', post, {
        headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    return response.data;
}

export async function putPost(id, post) {
    const token = await authService.getAccessToken();
    const response = await axios.put(`api/post/${id}`, post, {
        headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    return response.data;
}

export async function deletePost(id) {
    const token = await authService.getAccessToken();
    const response = await axios.delete(`api/post/${id}`, {
        headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    return response.data;
}