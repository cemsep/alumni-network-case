﻿import * as axios from 'axios';
import authService from '../components/api-authorization/AuthorizeService';

export async function getGroups() {
    const token = await authService.getAccessToken();
    const response = await axios.get('api/group', {
        headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    return response.data.map(g => {
        return { ...g, members: g.groupMembers && g.groupMembers.map(m => m.profileId) };
    });
}

export async function getGroup(groupId) {
    const token = await authService.getAccessToken();
    const response = await axios.get('api/group/' + groupId, {
        headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    return { ...response.data, members: response.data.groupMembers && response.data.groupMembers.map(m => m.profileId) };
}

export async function joinGroup(groupId, userIds) {
    const token = await authService.getAccessToken();
    let response = await axios.post(`api/group/${groupId}/join`, userIds || [], {
        headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    return { ...response.data, members: response.data.groupMembers && response.data.groupMembers.map(m => m.profileId) };
}

export async function leaveGroup(groupId) {
    const token = await authService.getAccessToken();
    const response = await axios.delete('api/group/' + groupId + '/leave', {
        headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    return { ...response.data, members: response.data.groupMembers && response.data.groupMembers.map(m => m.profileId) };
}

export async function postGroup(group) {
    const token = await authService.getAccessToken();
    const response = await axios.post('api/group', group, {
        headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    return { ...response.data, members: response.data.groupMembers && response.data.groupMembers.map(m => m.profileId) };
}