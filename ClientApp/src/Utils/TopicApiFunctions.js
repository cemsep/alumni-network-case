import * as axios from 'axios';
import authService from '../components/api-authorization/AuthorizeService';

export async function getTopics() {
    const token = await authService.getAccessToken();
    const response = await axios.get('api/topic', {
        headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    return response.data;
}

export async function getTopic(topicId) {
    const token = await authService.getAccessToken();
    const response = await axios.get('api/topic/' + topicId, {
        headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    return response.data;
}

export async function joinTopic(topicId) {
    const token = await authService.getAccessToken();
    const urlForJoining = 'api/topic/' + topicId + '/join';
    const response = await axios.post(urlForJoining, {}, {
        headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    return response.data;
}

export async function leaveTopic(topicId) {
    const token = await authService.getAccessToken();
    const response = await axios.delete('api/topic/' + topicId + '/leave', {
        headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    return response.data;
}

export async function postTopic(topic) {
    const token = await authService.getAccessToken();
    const response = await axios.post('api/topic', topic, {
        headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    return response.data;
}