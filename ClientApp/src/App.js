import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import Home from './components/Pages/Home';
import AuthorizeRoute from './components/api-authorization/AuthorizeRoute';
import ApiAuthorizationRoutes from './components/api-authorization/ApiAuthorizationRoutes';
import { ApplicationPaths } from './components/api-authorization/ApiAuthorizationConstants';

import './custom.css'
import Groups from './components/Groups/Groups';
import Topics from './components/Topics/Topics';
import TopicViewPage from './components/Pages/TopicViewPage';
import PostViewPage from './components/Pages/PostViewPage';
import ProfileViewPage from './components/Pages/ProfileViewPage';
import ProfileSettingsViewPage from './components/Pages/ProfileSettingsViewPage';
import CalendarPage from './components/Pages/CalendarPage';
import EventViewPage from './components/Pages/EventViewPage';
import GroupPage from './components/Pages/GroupPage';

export default class App extends Component {
    static displayName = App.name;
	
    render () {
        return (
            <Layout>
                <AuthorizeRoute exact path='/' component={Home} />
                <AuthorizeRoute exact path='/post/:id' component={PostViewPage} />
                <AuthorizeRoute exact path='/event/:id' component={EventViewPage} />
				<AuthorizeRoute exact path='/groups' component={Groups} />
				<AuthorizeRoute exact path='/group/:groupId' component={GroupPage} />
                <AuthorizeRoute exact path='/topics' component={Topics} />
                <AuthorizeRoute exact path='/topic/:topicId' component={TopicViewPage} />
                <AuthorizeRoute exact path='/profile/:id' component={ProfileViewPage} />
                <AuthorizeRoute exact path='/settings/:id' component={ProfileSettingsViewPage} />
                <AuthorizeRoute exact path='/calendar' component={CalendarPage} />
                <Route path={ApplicationPaths.ApiAuthorizationPrefix} component={ApiAuthorizationRoutes} />
            </Layout>
        );
    }
}
