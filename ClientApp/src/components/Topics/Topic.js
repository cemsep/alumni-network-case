﻿import React, { Component } from 'react';
import Post from '../Posts/Post';
import Event from '../Events/Event';
import { connect } from 'react-redux';
import { topicSelector } from '../../Modules/Selectors/TopicSelectors';
import * as renderIf from 'render-if';
import topicActionDispatchers from '../../Modules/Actions/TopicActionDispatchers';
import './Topic.styles.scss';
import CreatePostModal from '../Modal/CreatePostModal';
import { Button } from 'reactstrap';
import TopicPosts from './TopicPosts';
import TopicEvents from './TopicEvents';

class Topic extends Component {

    state = { showModal: false }

    componentDidMount() {
        topicActionDispatchers.getTopicAction(this.props.id);
    }

    handleClickEvent = (event) => {
        this.setState({ showModal: true });
    }

    handleCloseModal = () => {
        this.setState({ showModal: false });
    }

    render() {
        return (
            <div>
                {renderIf(this.props.isLoading)(() => (
                    <div>Loading topics...</div>
                ))}
                {renderIf(!this.props.isLoading)(() => (
                    <div>
                        {renderIf(this.props.error)(() => (
                            <div>
                                There was an error getting topic
                                <br />
                                {this.props.error.message}
                            </div>
                        ))}
                        {renderIf(this.props.topic)(() => (
                            <div className='container'>
                                <div className='topicInfo'>
                                    <div className='topicName'>
                                        <h1>{this.props.topic.name}</h1>
                                    </div>
                                    <div className='topicDetails'>
                                        <h5>Details</h5>
                                        {this.props.topic.description}
                                    </div>
                                </div>
                                {renderIf(this.props.isMember)(() => (
                                    <div>
                                        <div className='allPosts'>
                                            <Button color="primary" onClick={this.handleClickEvent}>New Post</Button>
                                            <TopicPosts id={this.props.topic.id} />
                                        </div>
                                        <div className='allEvents'>
                                            <TopicEvents id={this.props.topic.id} />
                                        </div>
                                        <CreatePostModal show={this.state.showModal} targetTopicId={this.props.topic.id} handleClose={this.handleCloseModal} />
                                    </div>
                                ))}
                            </div>
                        ))}
                    </div>
                ))}
            </div>
        );
    }
}

export default connect(topicSelector)(Topic);