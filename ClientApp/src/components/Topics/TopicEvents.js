import React, { Component } from 'react';
import Event from '../Events/Event';
import { connect } from 'react-redux';
import * as renderIf from 'render-if';
import './Topic.styles.scss';
import { topicEventsSelector } from '../../Modules/Selectors/TopicSelectors';

class TopicEvents extends Component {
    render() {
        return (
            <div>
                {renderIf(this.props.events)(() => (
                    <div>
                        {this.props.events.map(e => <Event event={e} key={e.id}/>)}
                    </div>
                ))}
            </div>
        );
    }
}

export default connect(topicEventsSelector)(TopicEvents);