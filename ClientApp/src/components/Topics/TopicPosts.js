import React, { Component } from 'react';
import Post from '../Posts/Post';
import { connect } from 'react-redux';
import * as renderIf from 'render-if';
import './Topic.styles.scss';
import { topicPostsSelector } from '../../Modules/Selectors/PostSelectors';

class TopicPosts extends Component {
    
    render() {
        return (
            <div>
                {renderIf(this.props.posts)(() => (
                    <div>
                        {this.props.posts.map(p => <Post post={p} key={p.id}/>)}
                    </div>
                ))}
            </div>
        );
    }
}

export default connect(topicPostsSelector)(TopicPosts);