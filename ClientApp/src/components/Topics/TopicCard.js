import React, { Component } from 'react';
import {
  Card, CardText, CardBody,
  CardTitle, Button
} from 'reactstrap';
import { Link } from 'react-router-dom';
import * as renderIf from 'render-if';
import './TopicCard.styles.scss';
import authService from '../api-authorization/AuthorizeService';
import profileActionDispatchers from '../../Modules/Actions/ProfileActionDispatchers';


class TopicCard extends Component {

    state = { isMember: false };
    _isMounted = false;

    componentDidMount() {
      this._isMounted = true;
      this._isMounted && this.isMember(this.props.topic);
    }

    componentDidUpdate(prevProps) {
      if (prevProps.topic.topicMember !== this.props.topic.topicMember) {
        this._isMounted && this.isMember(this.props.topic);
      }
    }

    async isMember (topic) {
        const currentUser = await authService.getUser();
        const currentUserId = currentUser.sub;
        let memberOrNot = false;
        if (topic.topicMember) {
          for (var member of topic.topicMember) {
              if (member.profileId === currentUserId) {
                  memberOrNot = true;
              }
          }
          this._isMounted && this.setState({ isMember: memberOrNot });
        }
    }

    componentWillUnmount() {
      this._isMounted = false;
   }

    render() {
        const { topic } = this.props;

        return (
          <Card className="topicCard">
                <CardBody className="d-flex flex-column">
                    <div className={'divAroundLink' + topic.id}>
                        <Link to={{ pathname: '/topic/' + topic.id, state: { isMember: this.state.isMember } }} style={{ textDecoration: 'none' }} key={topic.id}>
                          <CardTitle>
                            {topic.name}
                          </CardTitle>
                          <CardText>
                            {topic.description}
                          </CardText>
                        </Link>
                    </div>
                    {renderIf(!this.state.isMember)(() => (
                      <Button className="mt-auto" onClick={() => this.props.onSubscribe(topic.id)} color="primary">Subscribe</Button>
                    ))}
                    {renderIf(this.state.isMember)(() => (
                      <Button className="mt-auto" onClick={() => this.props.onUnsubscribe(topic.id)} color="secondary">Unsubscribe</Button>
                    ))}
            </CardBody>
          </Card>
        );
    }
}

export default TopicCard;