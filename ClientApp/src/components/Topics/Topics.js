import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { topicsSelector } from '../../Modules/Selectors/TopicSelectors';
import * as renderIf from 'render-if';
import topicActionDispatchers from '../../Modules/Actions/TopicActionDispatchers';
import * as moment from 'moment';
import { Button, Card, CardText, CardBody, CardTitle } from 'reactstrap';
import { Link } from 'react-router-dom';
import CreateTopicModal from '../Modal/CreateTopicModal';
import TopicCard from './TopicCard';

class Topics extends Component {

    state = { showModal: false }

    componentDidMount() {
        topicActionDispatchers.getTopicsAction();
    }

    onSubscribe = (topicId) => {
        topicActionDispatchers.joinTopicAction(topicId);
    }

    onUnsubscribe = (topicId) => {
        topicActionDispatchers.leaveTopicAction(topicId);
    }

    handleClickEvent = (event) => {
        this.setState({ showModal: true });
    }

    handleCloseModal = () => {
        this.setState({ showModal: false });
    }

    render() {
      const styles = {
          display: 'grid',
          gridTemplateColumns: 'repeat(5, auto)',
          gridGap: '20px'
      }  
      
      return (
          <div>
              {renderIf(this.props.isLoading)(() => (
                  <div>Loading topics...</div>
              ))}
              {renderIf(!this.props.isLoading)(() => (
                  <div>
                      {renderIf(this.props.error)(() => (
                          <div>
                              There was an error getting topics
                              <br />
                              {this.props.error.message}
                          </div>
                      ))}
                      <Button color="primary" style={{marginBottom: '1.5em'}} onClick={this.handleClickEvent}>New Topic</Button>
                      {renderIf(this.props.topics)(() => (
                          <div style={styles}>
                                {this.props.topics.map(topic =>
                                    <TopicCard topic={topic} onSubscribe={this.onSubscribe} onUnsubscribe={this.onUnsubscribe} key={topic.id} />
                                )}
                          </div>
                      ))}
                      <CreateTopicModal show={this.state.showModal} handleClose={this.handleCloseModal} />
                  </div>
              ))}
          </div>
      );
    }
}

export default withRouter(connect(topicsSelector)(Topics));