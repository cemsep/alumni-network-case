﻿import React, { Component } from 'react';
import { connect } from 'react-redux';
import { timeLineSelector } from '../../Modules/Selectors/TimeLineSelectors';
import * as renderIf from 'render-if';
import postActionDispatchers from '../../Modules/Actions/PostActionDispatchers';
import eventActionDispatchers from '../../Modules/Actions/EventActionDispatchers';
import * as moment from 'moment';
import CreatePostModal from '../Modal/CreatePostModal';
import { Button } from 'reactstrap';
import Post from '../Posts/Post';
import Event from '../Events/Event';
import { Input } from 'reactstrap';
import './TimeLine.scss';

class TimeLine extends Component {

    state = { showModal: false, filterText: '' }

    componentDidMount() {
        postActionDispatchers.getPostsAction();
        eventActionDispatchers.getEventsAction();
    }

    handleClickEvent = (event) => {
        this.setState({ showModal: true });
    }

    handleCloseModal = () => {
        this.setState({ showModal: false });
    }
    handleChangeSearch = (e) => {
        this.setState({ filterText: e.target.value.toLowerCase() });
    }
    render() {
        return (
            <div>
                {renderIf(this.props.isLoading)(() => (
                    <div>Loading...</div>
                ))}
                {renderIf(!this.props.isLoading)(() => (
                    <div>
                        {renderIf(this.props.error)(() => (
                            <div>
                                There was an error:
                                <br />
                                {this.props.error.message}
                            </div>
                        ))}
                        {renderIf(this.props.postsAndEvents)(() => (
                            <div>
                                <Input type="text" id="searchFilter" placeholder="search..." onChange={this.handleChangeSearch} />
                                <button onClick={this.handleClickEvent} className='btn btn-primary timeLineNewPostBtn'>New Post</button>
                                {this.props.postsAndEvents.map((postOrEvent, i) => {
                                    if (postOrEvent.createdOn) {
                                        return <Post post={postOrEvent} key={i} filterText={this.state.filterText} />
                                    } else {
                                        return <Event event={postOrEvent} key={i} filterText={this.state.filterText} />
                                    }
                                }
                                )}
                            </div>
                        ))}
                        <CreatePostModal show={this.state.showModal} handleClose={this.handleCloseModal} />
                    </div>
                ))}
            </div>
        );
    }
}

export default connect(timeLineSelector)(TimeLine);