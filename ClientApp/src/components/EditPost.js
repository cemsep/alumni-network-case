﻿import React, { Component } from 'react';
import * as renderIf from 'render-if';
import postActionDispatchers from './../Modules/Actions/PostActionDispatchers';
import TextArea from './Assets/TextArea';
import './EditPost.styles.scss';
import PreviewModal from './Modal/PreviewModal';
import 'bootstrap/dist/css/bootstrap.min.css';

class EditPost extends Component {
    constructor(props) {
        super(props);
        this.state = {
            post: {
                ...props.post
            },
            isPreviewOpen: false
        };
    }
    handleInputChange = e => {
        const { target } = e;
        const newPost = { ...this.state.post, [target.name]: target.value };
        this.setState({ post: newPost });
    }
    handleSubmit = e => {
        e.preventDefault();
        postActionDispatchers.putPostAction(this.props.post.id, this.state.post);
        this.props.toggle();
    }
    handleCancel = () => {
        this.setState({ post: { ...this.props.post } });
        this.props.toggle();
    }
    handlePreview = (e) => {
        e.preventDefault();
        this.setState({ isPreviewOpen: !this.state.isPreviewOpen });
    }
    render() {
        return (
            <div>
                <div>
                    <form onSubmit={this.handleSubmit}>
                        {renderIf(!this.props.post.replyParentId)(() => (
                            <input placeholder="Title..." name="title" value={this.state.post.title} onChange={this.handleInputChange} className="inputTitle" />
                        ))}
                        <TextArea placeholder="Message..." name="details" value={this.state.post.details} required onChange={this.handleInputChange} />
                        <div className="btnGroup">
                            <button onClick={this.handlePreview} className="previewButton submitButton btn btn-light">Preview</button>
                            <button onClick={this.handleCancel} className="submitButton btn btn-light">Cancel</button>
                            <button type="submit" className="submitButton btn btn-primary">Save Changes</button>
                        </div>
                    </form>
                    <PreviewModal post={this.state.post} show={this.state.isPreviewOpen} handleClose={this.handlePreview} />
                </div>
            </div>
        );
    }
}

export default EditPost;