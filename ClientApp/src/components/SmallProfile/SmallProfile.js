﻿import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as renderIf from 'render-if';
import { profileSelector } from '../../Modules/Selectors/ProfileSelectors';
import defaultProfilePicture from '../Assets/images/defaultUser.png';
import './SmallProfile.scss';

class SmallProfile extends Component {

    render() {
        const { profile } = this.props;
        return (
            <div>
                {renderIf(this.props.profile)(() => (
                    <a className='smallProfile' href={`/profile/${profile.userId}`}>
                        {renderIf(profile.profilePicture)(() => (
                            <img src={`data:image/jpeg;base64,${profile.profilePicture}`} alt="profile" className='smallProfileImg' />
                        ))}
                        {renderIf(!profile.profilePicture)(() => (
                            <img src={defaultProfilePicture} alt="profile" className='smallProfileImg' />
                        ))}
                        <div className='smallProfileName'>{profile.userName}</div>
                    </a>
                ))}
            </div> 
        );
    }
}

export default connect(profileSelector)(SmallProfile)
