﻿import React, { Component } from 'react';
import { Calendar, momentLocalizer, Views } from 'react-big-calendar'
import moment from 'moment';
import { connect } from 'react-redux';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import { calendarSelector } from '../../Modules/Selectors/EventSelectors';
import eventActionDispatchers from '../../Modules/Actions/EventActionDispatchers';
import 'moment/locale/en-gb'
import CreateEventModal from '../Modal/CreateEventModal';
import { history } from '../..';

const localizer = momentLocalizer(moment);

class CalendarView extends Component {
    constructor(props) {
        super(props);
        this.state = { currentView: Views.MONTH, start: null, end: null, showModal: false }
    }

    componentDidMount() {
        eventActionDispatchers.getEventsAction();
    }

    handleSelectEvent = ({ start, end }) => {
        let startTime = moment(start);
        let endTime = moment(end);
        if (this.state.currentView === Views.MONTH) {
            startTime = startTime.add(8, 'h');
            endTime = endTime.add(16, 'h');
        }
        this.setState({ start: startTime.toDate(), end: endTime.toDate(), showModal: true });
    }
    handleClickEvent = (event) => {
        history.push(`event/${event.id}`);
    }
    onView = (view) => {
        this.setState({ currentView: view });
    }
    handleCloseModal = () => {
        this.setState({ start: null, end: null, showModal: false });
    }

    eventStyleGetter = ({ isMember }) => {
        let style = {
            backgroundColor: '#6FAE89',
        };
        if (!isMember) {
            return;
        }
        return {
            style: style
        };
    };
    render() {
        return (
            <div>
                Click event to view details or click and drag to create a new event.
                <Calendar
                    culture='en-GB'
                    localizer={localizer}
                    events={this.props.events}
                    onView={this.onView}
                    style={{ height: 1200 }}
                    defaultDate={new Date()}
                    defaultView="month"
                    onSelectSlot={this.handleSelectEvent}
                    onSelectEvent={this.handleClickEvent}
                    eventPropGetter={this.eventStyleGetter}
                    selectable
                />
                <CreateEventModal start={this.state.start} end={this.state.end} show={this.state.showModal} handleClose={this.handleCloseModal}/>
            </div>
        );
    }
}

export default connect(calendarSelector)(CalendarView);

