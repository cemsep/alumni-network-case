import React, { Component } from 'react';
import { connect } from 'react-redux';
import { eventsSelector } from '../../Modules/Selectors/EventSelectors';
import * as renderIf from 'render-if';
import eventActionDispatchers from '../../Modules/Actions/EventActionDispatchers';
import * as moment from 'moment';
//import CreateNewEvent from './CreateNewEvent';
import Event from './Event';

class Events extends Component {

    componentDidMount() {
        eventActionDispatchers.getEventsAction();
    }

    render() {
      return (
          <div>
              {renderIf(this.props.isLoading)(() => (
                  <div>Loading events...</div>
              ))}
              {renderIf(!this.props.isLoading)(() => (
                  <div>
                      {renderIf(this.props.error)(() => (
                          <div>
                              There was an error getting events
                              <br />
                              {this.props.error.message}
                          </div>
                      ))}
                      {renderIf(this.props.events)(() => (
                          <div>
                             {this.props.events.map(event =>
                               <Event event={event} key={event.id} />
                             )}
                          </div>
                      ))}
                  </div>
              ))}
          </div>
      );
    }
}

export default connect(eventsSelector)(Events);