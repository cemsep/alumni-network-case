import React, { Component } from 'react';
import * as moment from 'moment';
import './Event.styles.scss';
import { stringifyDateTime } from '../../Utils/HelperFunctions';
import { history } from '../..';
import * as renderIf from 'render-if';

const defaultEventImage = require('../Assets/images/default-event-thumb.png');

class Event extends Component {

    searchHit(filterText) {
        const { event } = this.props;
        if (!filterText) {
            return true;
        }
        if (event.name.toLowerCase().includes(filterText) || event.description.toLowerCase().includes(filterText)) {
            return true;
        }
        return false;
    }

    render() {
        const { event } = this.props;
        const startTime = new Date(moment.utc(event.startTime)).toDateString().split(' ');
        const dateTime = stringifyDateTime(moment.utc(event.startTime));

        return (
            <div>
                {renderIf(this.searchHit(this.props.filterText))(() => (
                    <div className="eventBox">
                        <div className="eventHeader">
                            <img className="eventImage" src={defaultEventImage} alt="event" />
                            <div className="eventDate">
                                <h5 className="eventStartTime">{startTime[1]}</h5>
                                <h5 className="eventStartTime">{startTime[2]}</h5>
                            </div>
                            <div className="eventHeaderContent">
                                <h4 className="eventName" onClick={() => history.push(`/event/${event.id}`)}>{event.name}</h4>
                                <div className="eventLastUpdates">Last updated: {moment.utc(event.lastUpdated).fromNow()}</div>
                            </div>
                        </div>
                        <div className="eventBody">
                            <div>{dateTime}</div>
                            <div className="eventDescription">{event.description}</div>
                        </div>
                    </div>
                ))}
            </div>
        );
    }
}

export default Event;