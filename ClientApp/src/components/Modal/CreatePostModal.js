import React, { Component } from 'react';
import { Modal, Button, ModalBody, ModalFooter, ModalHeader, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import 'react-datepicker/dist/react-datepicker.css';
import './CreatePostModal.styles.scss';
import postActionDispatchers from '../../Modules/Actions/PostActionDispatchers';
import renderIf from 'render-if';
import ProfileSelect from '../Assets/ProfileSelect';
import GroupSelect from '../Assets/GroupSelect';
import TopicSelect from '../Assets/TopicSelect';
import PreviewModal from './PreviewModal';

class CreatePostModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            post: props.post ? props.post : {
                title: '', details: '', replyParentId: props.replyParentId, targetUserId: props.targetUserId, targetGroupId: props.targetGroupId, targetTopicId: props.targetTopicId
            },
            showPreview: false,
            target: 'public'
        };
        this.formRef = React.createRef();
    }
    handleInputChange = e => {
        const { target } = e;
        const newPost = { ...this.state.post, [target.name]: target.value };
        this.setState({ post: newPost });
    }
    handlePreview = () => {
        this.setState({ showPreview: !this.state.showPreview });
    }
    createPost = (e) => {
        e.preventDefault();
        if (this.props.edit) {
            postActionDispatchers.putPostAction(this.props.post.id, this.state.post);
        } else {
            postActionDispatchers.postPostAction(this.state.post);
        }
        this.props.handleClose();
    }
    handleChangeTargetType(target) {
        this.setState({ target, post: { ...this.state.post, targetGroupId: null, targetUserId: null, targetTopicId: null } });
    }
    handleTargetUser = (targetUser) => {
        this.setState({ post: { ...this.state.post, targetUserId: targetUser.id } });
    }
    handleTargetGroup = (targetGroup) => {
        this.setState({ post: { ...this.state.post, targetGroupId: targetGroup.id } })
    }
    handleTargetTopic = (targetTopic) => {
        this.setState({ post: { ...this.state.post, targetTopicId: targetTopic.id } })
    }
    targetPreSelected() {
        return this.props.targetUserId || this.props.targetTopicId || this.props.targetGroupId;
    }
    render() {
        return (
            <div>
                <Modal size='lg' isOpen={this.props.show} fade={false} toggle={this.props.handleClose}>
                    <ModalHeader toggle={this.props.handleClose}>{'Create New Post'}</ModalHeader>
                    <ModalBody>
                        <Form onSubmit={this.createPost}>
                            {renderIf(!this.props.edit && !this.targetPreSelected())(() => (
                                <div>
                                    Send to:
                                    <div className='radioButtons'>
                                        <FormGroup check className='targetRadioButton'>
                                            <Label check>
                                                <Input type="radio" name="public" checked={this.state.target === 'public'} onChange={() => this.handleChangeTargetType('public')} />{' '}
                                                Public
                                    </Label>
                                        </FormGroup>
                                        <FormGroup check className='targetRadioButton'>
                                            <Label check>
                                                <Input type="radio" name="targetUser" checked={this.state.target === 'user'} onChange={() => this.handleChangeTargetType('user')} />{' '}
                                                User
                                    </Label>
                                        </FormGroup>
                                        <FormGroup check className='targetRadioButton'>
                                            <Label check>
                                                <Input type="radio" name="targetGroup" checked={this.state.target === 'group'} onChange={() => this.handleChangeTargetType('group')} />{' '}
                                                Group
                                    </Label>
                                        </FormGroup>
                                        <FormGroup check className='targetRadioButton'>
                                            <Label check>
                                                <Input type="radio" name="targetTopic" checked={this.state.target === 'topic'} onChange={() => this.handleChangeTargetType('topic')} />{' '}
                                                Topic
                                    </Label>
                                        </FormGroup>
                                    </div>
                                    {renderIf(this.state.target === 'user')(() => (
                                        <ProfileSelect onChange={this.handleTargetUser} />
                                    ))}
                                    {renderIf(this.state.target === 'group')(() => (
                                        <GroupSelect onChange={this.handleTargetGroup} />
                                    ))}
                                    {renderIf(this.state.target === 'topic')(() => (
                                        <TopicSelect onChange={this.handleTargetTopic} />
                                    ))}
                                </div>
                            ))}
                            <FormGroup>
                                <Label for="title">Title</Label>
                                <Input type="title" 
                                    placeholder="Title..." 
                                    name="title" 
                                    defaultValue={this.props.post ? this.props.post.title : this.state.post.title} 
                                    onChange={this.handleInputChange} />
                                <Label className="label">Details</Label>
                                <Input type="textarea"
                                    placeholder="Message..."
                                    name="details"
                                    defaultValue={this.props.post ? this.props.post.details : this.state.post.details}
                                    onChange={this.handleInputChange}
                                    onKeyPress={e => {
                                        if (e.key === "Enter" && !e.shiftKey) {
                                            this.createPost(e);
                                        }
                                    }}
                                    />
                            </FormGroup>
                        </Form>
                    </ModalBody>
                    <ModalFooter>
                        <PreviewModal post={this.state.post} show={this.state.showPreview} handleClose={this.handlePreview} />
                        <Button color="secondary" onClick={this.props.handleClose}>Cancel</Button>
                        <Button color="secondary" onClick={this.handlePreview}>Preview</Button>
                        {renderIf(this.props.edit)(() => (
                            <Button color="primary" onClick={this.createPost}>Save Changes</Button>
                        ))}
                        {renderIf(!this.props.edit)(() => (
                            <Button color="primary" onClick={this.createPost}>Create Post</Button>
                        ))}
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

export default CreatePostModal