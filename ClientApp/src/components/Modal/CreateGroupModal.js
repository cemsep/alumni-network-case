import React, { Component } from 'react';
import { Modal, Button, ModalBody, ModalFooter, ModalHeader, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import 'react-datepicker/dist/react-datepicker.css';
import './CreateGroupModal.styles.scss';
import groupActionDispatchers from '../../Modules/Actions/GroupActionDispatchers';

class CreateGroupModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            group: { 
              name: '', description: '', isPrivate: false 
            },
            isCollapsed: true
        };
        this.formRef = React.createRef();
    }
    handleInputChange = e => {
        const { target } = e;
        const newGroup = { ...this.state.group, [target.name]: target.value };
        this.setState({ group: newGroup });
    }
    handleCheckboxChange = e => {
        const { target } = e;
        const newGroup = { ...this.state.group, [target.name]: target.checked };
        this.setState({ group: newGroup });
    }
    createGroup = () => {
        groupActionDispatchers.postGroupAction(this.state.group);
        this.props.handleClose();
    }

    render() {
        return (
            <div>
                <Modal size='lg' isOpen={this.props.show} fade={false} toggle={this.props.handleClose}>
                    <ModalHeader toggle={this.props.handleClose}>{'Create New Group'}</ModalHeader>
                    <ModalBody>
                        <Form>
                            <FormGroup>
                                <Label for="name">Name</Label>
                                <Input type="title" placeholder="Name..." name="name" onChange={this.handleInputChange} />
                                <Label className="label">Description</Label>
                                <Input type="textarea" placeholder="Description..." name="description" onChange={this.handleInputChange} />
                            </FormGroup>
                            <FormGroup check>
                                <Label check>
                                    <Input type="checkbox" id="isPrivate" name="isPrivate" value={this.state.isPrivate} onChange={this.handleCheckboxChange} /> Private group
                                </Label>
                            </FormGroup>
                        </Form>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.props.handleClose}>Cancel</Button>
                        <Button color="primary" onClick={this.createGroup}>Create Group</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

export default CreateGroupModal