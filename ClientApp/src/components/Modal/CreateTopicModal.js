import React, { Component } from 'react';
import { Modal, Button, ModalBody, ModalFooter, ModalHeader, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import './CreateTopicModal.styles.scss';
import topicActionDispatchers from '../../Modules/Actions/TopicActionDispatchers';

class CreateTopicModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            topic: {
                name: '', description: ''
            },
            isCollapsed: true
        };
        this.formRef = React.createRef();
    }
    handleInputChange = e => {
        const { target } = e;
        const newTopic = { ...this.state.topic, [target.name]: target.value };
        this.setState({ topic: newTopic });
    }
    createTopic = () => {
        topicActionDispatchers.postTopicAction(this.state.topic);
        this.props.handleClose();
  }

    render() {
        return (
            <div>
                <Modal size='lg' isOpen={this.props.show} fade={false} toggle={this.props.handleClose}>
                    <ModalHeader toggle={this.props.handleClose}>{'Create New Topic'}</ModalHeader>
                    <ModalBody>
                        <Form>
                            <FormGroup>
                                <Label for="name">Name</Label>
                                <Input type="title" placeholder="Name..." name="name" onChange={this.handleInputChange} />
                                <Label className="label">Description</Label>
                                <Input type="textarea" placeholder="Description..." name="description" onChange={this.handleInputChange} />
                            </FormGroup>
                        </Form>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.props.handleClose}>Cancel</Button>
                        <Button color="primary" onClick={this.createTopic}>Create Topic</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

export default CreateTopicModal