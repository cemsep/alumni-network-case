﻿import React, { Component } from 'react';
import { Modal, Button, ModalBody, ModalFooter, ModalHeader, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import './CreateEventModal.styles.scss';
import moment from 'moment';
import eventActionDispatchers from '../../Modules/Actions/EventActionDispatchers';
import { fileToBase64 } from '../../Utils/ImageConverter';

class CreateEventModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            event: {
                name: '', description: '', startTime: props.start, endTime: props.end, bannerImg: []
            },
            isCollapsed: true,
        };
        this.formRef = React.createRef();
    }

    componentDidMount() {
        this.setState({
            event: {
                title: '', description: '', startTime: this.props.start, endTime: this.props.end, bannerImg: []
            }
        });
    }

    componentDidUpdate(prevProps) {
        if (prevProps !== this.props) {
            this.setState({
                event: {
                    title: '', description: '', startTime: this.props.start, endTime: this.props.end, bannerImg: []
                }
            });
        }
    }

    handleInputChange = e => {
        const { target } = e;
        const newEvent = { ...this.state.event, [target.name]: target.value };
        this.setState({ event: newEvent });
    }

    handleImageChange = e => {
        const { target } = e;
        fileToBase64(target.files[0]).then(result => {
          const base64 = result.split(',');
          const newEvent = { ...this.state.event, bannerImg: base64[1] };
          this.setState({ event: newEvent });
        });
      }

    handleInputStart = (e) => {
        const newEvent = { ...this.state.event, startTime: e };
        this.setState({ event: newEvent });
    }

    handleInputEnd = (e) => {
        const newEvent = { ...this.state.event, endTime: e };
        this.setState({ event: newEvent });
    }

    createEvent = () => {
        eventActionDispatchers.postEventAction(this.state.event);
        this.props.handleClose();
    }

    render() {
        return (
            <div>
                <Modal size='lg' isOpen={this.props.show} fade={false} toggle={this.props.handleClose}>
                    <ModalHeader toggle={this.props.handleClose}>{'Create New Event'}</ModalHeader>
                    <ModalBody>
                        <Form>
                            <FormGroup>
                                <Label for="name">Name</Label>
                                <Input type="title" name="name" placeholder="name.." onChange={this.handleInputChange} />
                                <Label className="label">Start time</Label>
                                <div className="dateInput">
                                    <DatePicker
                                        name="startTime"
                                        selected={this.state.event.startTime}
                                        onChange={this.handleInputStart}
                                        showTimeSelect
                                        timeFormat="HH:mm"
                                        timeIntervals={15}
                                        timeCaption="time"
                                        dateFormat="HH:mm, d MMMM, yyyy"
                                    />
                                </div>
                                <Label className="label">End time</Label>
                                <div className="dateInput">
                                    <DatePicker
                                        name="endTime"
                                        selected={this.state.event.endTime}
                                        onChange={this.handleInputEnd}
                                        showTimeSelect
                                        timeFormat="HH:mm"
                                        timeIntervals={15}
                                        timeCaption="time"
                                        dateFormat="HH:mm, d MMMM, yyyy"
                                    />
                                </div>
                                <Label className="label">Description</Label>
                                <Input type="textarea" placeholder="Description..." name="description" value={this.state.details} onChange={this.handleInputChange} />
                            </FormGroup>
                            <FormGroup>
                                <Label for="eventPicture">Event Picture</Label>
                                <Input type="file" name="eventPicture" id="eventPicture" onChange={this.handleImageChange}/>
                                <FormText color="muted">
                                    Upload an image.
                                </FormText>
                            </FormGroup>
                        </Form>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.props.handleClose}>Cancel</Button>
                        <Button color="primary" onClick={this.createEvent}>Create Event</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

export default CreateEventModal