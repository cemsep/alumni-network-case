﻿import React, { Component } from 'react';
import { Modal, Button, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import ReactMarkdown from 'react-markdown';
import 'github-markdown-css';

class PreviewModal extends Component {

	render() {
		return (
			<div>
                <Modal size='lg' isOpen={this.props.show} fade={false} toggle={this.props.handleClose}>
                    <ModalHeader toggle={this.props.handleClose}>{this.props.post.title || 'Preview'}</ModalHeader>
                    <ModalBody className='markdown-body'>
                        <ReactMarkdown source={this.props.post.details} />
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={this.props.handleClose}>Close</Button>
                    </ModalFooter>
                </Modal>
			</div>
		);
	}
}

export default PreviewModal