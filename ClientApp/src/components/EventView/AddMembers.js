﻿import React, { Component } from 'react';
import * as renderIf from 'render-if';
import { history } from '../..';
import ProfileSelect from '../Assets/ProfileSelect';
import eventActionDispatchers from '../../Modules/Actions/EventActionDispatchers';
import './AddMembers.styles.scss';

class Addmembers extends Component {
    constructor(props) {
        super(props);
        this.state = { selected: [] }
    }

    onChange = values => {
        this.setState({ selected: (values && values.map(v => v.id)) || [] });
    }

    handleAddMembersClick = () => {
        eventActionDispatchers.joinEventAction(this.props.eventId, this.state.selected);
    }

    render() {
        const buttonDisabled = this.state.selected.length === 0
        return (
            <div className='addMembers'>
                <div className='addMembersSelect'>
                    <ProfileSelect isMulti={true} filter={this.props.members} onChange={this.onChange}/>
                </div>
                <button className={'btn btn-primary addMembersBtn' + (buttonDisabled ? ' addMembersBtnDisabled' : '')} disabled={buttonDisabled} onClick={this.handleAddMembersClick}>Add Members</button>
            </div>
        );
    }
}

export default Addmembers;