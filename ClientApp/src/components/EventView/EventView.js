﻿import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as renderIf from 'render-if';
import * as moment from 'moment';
import ReactMarkdown from 'react-markdown';
import eventActionDispatchers from '../../Modules/Actions/EventActionDispatchers';
import { eventSelector } from '../../Modules/Selectors/EventSelectors';
import SmallProfile from '../SmallProfile/SmallProfile';
import './EventView.styles.scss';
import ProfileNameLink from './ProfileNameLink';
import ProfileSelect from '../Assets/ProfileSelect';
import Addmembers from './AddMembers';
import EventBanner from '../Assets/EventBanner';

class EventView extends Component {

    componentDidMount() {
        eventActionDispatchers.getEventAction(this.props.id);
    }

    render() {
        const { event } = this.props;
        return (
            <div>
                {renderIf(this.props.isLoading)(() => (
                    <div>Loading...</div>))}
                {renderIf(event && !this.props.isLoading)(() => (
                    <div>
                        <div className='eventBanner'>
                            <EventBanner id={event.id} className='eventBannerImg' />
                        </div>
                        <div className='eventViewHeader'>
                            <h1 className='eventViewTitle'>{event.name}</h1>
                            <div className='eventSubHeader'>
                                <div>{event.allowGuests ? 'Public' : 'Private'} - Created by <ProfileNameLink id={event.createdById} /> - Last updated {moment.utc(event.lastUpdated).local().fromNow()}</div>
                                {renderIf(!this.props.isMember)(() => (
                                    <button onClick={() => eventActionDispatchers.joinEventAction(event.id)} className='btn btn-primary eventJoinBtn'>Join Event</button>
                                ))}
                            </div>
                        </div>
                        <div className='timeTitle'>Starts at:</div>
                        <div className='timeValue'>{moment.utc(event.startTime).local().format('LLLL')}</div>
                        <div className='timeTitle'>Ends at:</div>
                        <div className='timeValue'>{moment.utc(event.endTime).local().format('LLLL')}</div>
                        <div className='eventViewDescriptionTitle'>Description:</div>
                        <div className='eventViewDescription'>{event.description}</div>
                        {renderIf(event.createdById === this.props.userId)(() => (
                            <div>
                                <div className='eventViewAddMembersTitle'>Add new members:</div>
                                <Addmembers eventId={event.id} members={event.members} />
                            </div>
                        ))}
                        {renderIf(event.members)(() => (
                            <div>
                                <div className='eventViewMembers'>Members:</div>
                                {event.members.map(member => (<SmallProfile key={member} id={member} />))}
                            </div>
                            ))}
                    </div>
                ))}
            </div>
        );
    }
}

export default connect(eventSelector)(EventView);