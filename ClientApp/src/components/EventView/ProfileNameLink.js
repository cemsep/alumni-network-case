﻿import React, { Component } from 'react';
import * as renderIf from 'render-if';
import * as moment from 'moment';
import profileActionDispatchers from '../../Modules/Actions/ProfileActionDispatchers';
import { profileSelector } from '../../Modules/Selectors/ProfileSelectors';
import { connect } from 'react-redux';

class ProfileNameLink extends Component {
    componentDidMount() {
        profileActionDispatchers.getProfileAction(this.props.id);
    }
    render() {
        return (
            <a className='profileLink' href={`/profile/${this.props.id}`}>
                {this.props.profile ? this.props.profile.userName : this.props.id}
            </a>
        );
    }
}

export default connect(profileSelector)(ProfileNameLink);