﻿import React, { Component } from 'react';
import Post from '../Posts/Post';
import Event from '../Events/Event';
import { connect } from 'react-redux';
import { groupSelector } from '../../Modules/Selectors/GroupSelectors';
import * as renderIf from 'render-if';
import groupActionDispatchers from '../../Modules/Actions/GroupActionDispatchers';
import './Group.styles.scss';
import GroupPosts from './GroupPosts';
import SmallProfile from '../SmallProfile/SmallProfile';
import AddMemberToGroup from './AddMemberToGroup';
import CreatePostModal from '../Modal/CreatePostModal';
import { Button } from 'reactstrap';

class Group extends Component {

    state = { showModal: false }

    componentDidMount() {
        groupActionDispatchers.getGroupAction(this.props.id);
    }

    handleClickEvent = (event) => {
        this.setState({ showModal: true });
    }

    handleCloseModal = () => {
        this.setState({ showModal: false });
    }

    render() {
        return (
            <div>
                {renderIf(this.props.isLoading)(() => (
                    <div>Loading groups...</div>
                ))}
                {renderIf(!this.props.isLoading)(() => (
                    <div>
                        {renderIf(this.props.error)(() => (
                            <div>
                                There was an error getting group
                                <br />
                                {this.props.error.message}
                            </div>
                        ))}
                        {renderIf(this.props.group)(() => (
                            <div className='container'>
                                <div className='groupInfo'>
                                    <div className='groupName'>
                                        <h1>{this.props.group.name}</h1>
                                    </div>
                                    <div className='groupDetails'>
                                        <h5>Details</h5>
                                        {this.props.group.description}
                                    </div>
                                </div>
                                {renderIf(this.props.isMember)(() => (
                                    <div>
                                        <div>Add members:</div>
                                        <AddMemberToGroup groupId={this.props.id} members={this.props.group.members} />
                                    </div>
                                ))}
                                {renderIf(this.props.group.members)(() => (
                                    <div>
                                        <div className='eventViewMembers'>Members:</div>
                                        {this.props.group.members.map(member => (<SmallProfile key={member} id={member} />))}
                                    </div>
                                ))}
                                {renderIf(this.props.isMember)(() => (
                                    <div>
                                        <div className='allPosts'>
                                            <Button color="primary" onClick={this.handleClickEvent}>New Post</Button>
                                            <GroupPosts id={this.props.group.id} />
                                        </div>
                                        <CreatePostModal show={this.state.showModal} targetGroupId={this.props.group.id} handleClose={this.handleCloseModal} />
                                    </div>
                                ))}
                            </div>
                        ))}
                    </div>
                ))}
            </div>
        );
    }
}

export default connect(groupSelector)(Group);