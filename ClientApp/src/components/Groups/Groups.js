﻿import React, { Component } from 'react';
import { connect } from 'react-redux';
import { groupsSelector } from '../../Modules/Selectors/GroupSelectors';
import * as renderIf from 'render-if';
import groupActionDispatchers from '../../Modules/Actions/GroupActionDispatchers';
import './Groups.styles.scss';
import { Link } from 'react-router-dom';
import CreateGroupModal from '../Modal/CreateGroupModal';
import {
    Card, CardText, CardBody, Col, Row, Button
  } from 'reactstrap';
import authService from '../api-authorization/AuthorizeService';
import GroupCard from './GroupCard';

class Groups extends Component {

    state = { showModal: false }

    componentDidMount() {
        groupActionDispatchers.getGroupsAction();
    }

    joinGroup = (groupId) => {
        groupActionDispatchers.joinGroupAction(groupId, [this.props.userId]);
    }

    leaveGroup = (groupId) => {
        groupActionDispatchers.leaveGroupAction(groupId);
    }

    handleClickEvent = (event) => {
        this.setState({ showModal: true });
    }

    handleCloseModal = () => {
        this.setState({ showModal: false });
    }

    render() {
        return (
            <div>
                {renderIf(this.props.isLoading)(() => (
                    <div>Loading groups...</div>
                ))}
                {renderIf(!this.props.isLoading)(() => (
                    <div>
                        {renderIf(this.props.error)(() => (
                            <div>
                                There was an error getting groups
                                <br />
                                {this.props.error.message}
                            </div>
                        ))}
                        {renderIf(this.props.groups)(() => (
                            <div>
                                <Button color="primary" onClick={this.handleClickEvent}>New Group</Button>
                                <div className='allGroups'>
                                    {this.props.groups.map(group =>
                                        <GroupCard id={group.id} joinGroup={this.joinGroup} leaveGroup={this.leaveGroup} key={group.id} />
                                    )}    
                                </div>
                                <CreateGroupModal show={this.state.showModal} handleClose={this.handleCloseModal} />
                            </div>
                        ))}
                    </div>
                ))}
            </div>
        );
    }
}

export default connect(groupsSelector)(Groups);