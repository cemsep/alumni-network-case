﻿import React, { Component } from 'react';
import ProfileSelect from '../Assets/ProfileSelect';
import '../EventView/AddMembers.styles.scss';
import groupActionDispatchers from '../../Modules/Actions/GroupActionDispatchers';

class AddmemberToGroup extends Component {
    constructor(props) {
        super(props);
        this.state = { selected: [] }
    }

    onChange = values => {
        this.setState({ selected: (values && values.map(v => v.id)) || [] });
    }

    handleAddMembersClick = () => {
        groupActionDispatchers.joinGroupAction(this.props.groupId, this.state.selected);
    }

    render() {
        const buttonDisabled = this.state.selected.length === 0
        return (
            <div className='addMembers'>
                <div className='addMembersSelect'>
                    <ProfileSelect isMulti filter={this.props.members} onChange={this.onChange} />
                </div>
                <button className={'btn btn-primary addMembersBtn' + (buttonDisabled ? ' addMembersBtnDisabled' : '')} disabled={buttonDisabled} onClick={this.handleAddMembersClick}>Add Members</button>
            </div>
        );
    }
}

export default AddmemberToGroup;