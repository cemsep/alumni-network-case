﻿import React, { Component } from 'react';
import Post from '../Posts/Post';
import { connect } from 'react-redux';
import * as renderIf from 'render-if';
import './Group.styles.scss';
import { groupPostsSelector } from '../../Modules/Selectors/PostSelectors';

class GroupPosts extends Component {
    render() {
        return (
            <div>
                {renderIf(this.props.posts)(() => (
                    <div>
                        {this.props.posts.map(p => <Post post={p} key={p.id}/>)}
                    </div>
                ))}
            </div>
        );
    }
}

export default connect(groupPostsSelector)(GroupPosts);