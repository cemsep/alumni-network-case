import React, { Component } from 'react';
import * as renderIf from 'render-if';
import './Groups.styles.scss';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { groupSelector } from '../../Modules/Selectors/GroupSelectors';
import {
    Card, CardText, CardBody, Col, Row, Button
  } from 'reactstrap';
import groupActionDispatchers from '../../Modules/Actions/GroupActionDispatchers';


class GroupCard extends Component {

    render() {
        const { group } = this.props;

        return (
          <div key={'linkAndJoinButton_' + group.id}>
              <Row>
                  <Col>
                      <Link to={'/group/' + group.id} style={{ textDecoration: 'none' }} key={group.id}>
                          <Card className="groupCard">
                              <CardBody>{group.name}</CardBody>
                          </Card>
                      </Link>
                  </Col>
                  <Col>
                      {renderIf(!this.props.isMember)(() => (
                          <Button className='joinButton'
                              id={'buttonForGroup_' + group.id}
                              onClick={() => groupActionDispatchers.joinGroupAction(group.id)}
                                  key={'buttonToJoin' + group.id}>
                              Join
                          </Button>
                      ))}
                      {renderIf(this.props.isMember)(() => (
                          <Button className='leaveButton'
                              id={'buttonForGroup_' + group.id}
                              onClick={() => groupActionDispatchers.leaveGroupAction(group.id)}
                                  key={'buttonToLeave' + group.id}>
                              Leave
                          </Button>
                      ))}
                  </Col>
              </Row>
          </div>
        );
    }
}

export default connect(groupSelector)(GroupCard);