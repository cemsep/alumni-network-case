import React, { Component } from 'react';
import * as renderIf from 'render-if';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { profileSelector } from '../../Modules/Selectors/ProfileSelectors';
import profileActionDispatchers from '../../Modules/Actions/ProfileActionDispatchers';
import Settings from './Settings';

class ProfileSettings extends Component {
    componentDidMount() {
      profileActionDispatchers.getProfileAction(this.props.id);
    }
    render() {
      const { profile } = this.props;
      return (
        <div>
          {renderIf(profile)(() => (
            <Settings profile={profile}></Settings>
          ))}
        </div>
      );
    }
}

export default withRouter(connect(profileSelector)(ProfileSettings));