import React, { Component } from 'react';
import { connect } from 'react-redux';
import { profileSelector } from '../../Modules/Selectors/ProfileSelectors';
import { postsSelector } from '../../Modules/Selectors/PostSelectors';
import * as renderIf from 'render-if';
import authService from '../api-authorization/AuthorizeService';
import profileActionDispatchers from '../../Modules/Actions/ProfileActionDispatchers';
import postActionDispatchers from '../../Modules/Actions/PostActionDispatchers';
import * as moment from 'moment';
import {
  Card, CardText, CardBody,
  CardTitle, CardImg, CardHeader
} from 'reactstrap';
import './Profile.styles.scss';
import defaultProfilePicture from '../Assets/images/defaultUser.png';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit } from '@fortawesome/free-solid-svg-icons';
import { history } from '../..';
import Post from '../Posts/Post';


class Profile extends Component {

    componentDidMount() {
      profileActionDispatchers.getProfileAction(this.props.id);
      if (this.props.id === this.props.currentUserId)
        postActionDispatchers.getPostsAction(null, 'api/post/user')
    }

    componentDidUpdate(prevProps) {
      if (prevProps.id !== this.props.id) {
        profileActionDispatchers.getProfileAction(this.props.id);
        if (this.props.id === this.props.currentUserId)
          postActionDispatchers.getPostsAction(null, 'api/post/user')
      }
    }

    goToSettings() {
      history.push(`/settings/${this.props.profile.userId}`);
    }

    render() {
      const { profile } = this.props;
      const { currentUserId } = this.props;
      
      return (
          <div>
            {renderIf(profile)(() => (
              <div>
                <Card className="profileCard">
                  {renderIf(profile.profilePicture)(() => (
                    <CardImg top src={`data:image/jpeg;base64,${profile.profilePicture}`} alt="profile" />
                  ))}
                  {renderIf(!profile.profilePicture)(() => (
                    <CardImg top src={defaultProfilePicture} alt="profile" />
                  ))}
                  <CardBody className="userName">
                    {renderIf(profile.firstName && profile.lastName)(() => (
                      <CardTitle>
                        {profile.firstName} {profile.lastName}
                        {renderIf(profile.userId === currentUserId)(() => (
                          <FontAwesomeIcon className="icon" icon={faEdit} onClick={this.goToSettings.bind(this)} /> 
                        ))}
                      </CardTitle>
                    ))}
                    {renderIf(!profile.firstName || !profile.lastName)(() => (
                      <CardTitle>
                        {profile.userName}
                        {renderIf(profile.userId === currentUserId)(() => (
                          <FontAwesomeIcon className="icon" icon={faEdit} onClick={this.goToSettings.bind(this)} /> 
                        ))}
                      </CardTitle>
                    ))}       
                  </CardBody>
                  <CardHeader>Current work status</CardHeader>
                  {renderIf(profile.status)(() => (
                    <CardBody>
                      <CardText>{profile.status}</CardText>
                    </CardBody>
                  ))}
                  {renderIf(!profile.status)(() => (
                    <CardBody>
                      <CardText>No current status.</CardText>
                    </CardBody>
                  ))}
                  <CardHeader>Bio</CardHeader>
                  {renderIf(profile.bio)(() => (
                    <CardBody>
                      <CardText>{profile.bio}</CardText>
                    </CardBody>
                  ))}
                  {renderIf(!profile.bio)(() => (
                    <CardBody>
                      <CardText>No bio added.</CardText>
                    </CardBody>
                  ))}
                  <CardHeader>Fun fact about me</CardHeader>
                  {renderIf(profile.funFact)(() => (
                    <CardBody>
                      <CardText>{profile.funFact}</CardText>
                    </CardBody>
                  ))}
                  {renderIf(!profile.funFact)(() => (
                    <CardBody>
                      <CardText>No facts added.</CardText>
                    </CardBody>
                  ))}
                </Card>
                {renderIf(this.props.posts && this.props.id === this.props.currentUserId)(() => (
                    <div>
                        {this.props.posts.map(post =>
                            <Post post={post} key={post.id} />
                        )}
                    </div>
                ))}
              </div>
          ))}
        </div>
      );
    }
}

export default connect(profileSelector)(connect(postsSelector)(Profile));