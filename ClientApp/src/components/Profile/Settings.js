import React, { Component } from 'react';
import * as renderIf from 'render-if';
import profileActionDispatchers from '../../Modules/Actions/ProfileActionDispatchers';
import { Col, Row, Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import { fileToBase64 } from '../../Utils/ImageConverter';
import { history } from '../..';
//import './EditPost.styles.scss';

class Settings extends Component {
    constructor(props) {
        super(props);
        this.state = {
            profile: {
                ...props.profile
            }
        };
    }
    handleInputChange = e => {
        const { target } = e;
        var value = target.value;
        if (value === '')
          value = null;
        const newProfile = { ...this.state.profile, [target.name]: value };
        this.setState({ profile: newProfile });
    }
    handleImageChange = e => {
      const { target } = e;
      fileToBase64(target.files[0]).then(result => {
        const base64 = result.split(',');
        const newProfile = { ...this.state.profile, profilePicture: base64[1] };
        this.setState({ profile: newProfile });
      });
    }
    handleSubmit = e => {
        e.preventDefault();
        profileActionDispatchers.putProfileAction(this.props.profile.userId, this.state.profile);
        history.push(`/profile/${this.props.profile.userId}`);
    }
    render() {
      return (
          <div>
              <div>
                  <Form onSubmit={this.handleSubmit}>
                    <Row form>
                      <Col md={6}>
                        <FormGroup>
                          <Label for="firstName">First Name</Label>
                          <Input type="text" name="firstName" id="firstName" value={this.state.profile.firstName ? this.state.profile.firstName : ''} onChange={this.handleInputChange}/>
                        </FormGroup>
                      </Col>
                      <Col md={6}>
                        <FormGroup>
                          <Label for="lastName">Last Name</Label>
                          <Input type="text" name="lastName" id="lastName" value={this.state.profile.lastName ? this.state.profile.lastName : ''} onChange={this.handleInputChange}/>
                        </FormGroup>
                      </Col>
                    </Row>
                    <FormGroup>
                      <Label for="status">Work Status</Label>
                      <Input type="text" name="status" id="status" value={this.state.profile.status ? this.state.profile.status : ''} onChange={this.handleInputChange}/>
                    </FormGroup>
                    <FormGroup>
                      <Label for="bio">Bio</Label>
                      <Input type="textarea" name="bio" id="bio" value={this.state.profile.bio ? this.state.profile.bio : ''} onChange={this.handleInputChange}/>
                    </FormGroup>
                    <FormGroup>
                      <Label for="funFact">Fun Fact</Label>
                      <Input type="textarea" name="funFact" id="funFact" value={this.state.profile.funFact ? this.state.profile.funFact : ''} onChange={this.handleInputChange}/>
                    </FormGroup>
                    <FormGroup>
                      <Label for="profilePicture">Profile Picture</Label>
                      <Input type="file" name="profilePicture" id="profilePicture" onChange={this.handleImageChange}/>
                      <FormText color="muted">
                        Upload an image.
                      </FormText>
                    </FormGroup>
                    <div className="btnGroup">
                      <button type="submit" className="submitButton btn btn-primary">Save Changes</button>
                    </div>
                  </Form>
              </div>
          </div>
      );
    }
}

export default Settings;