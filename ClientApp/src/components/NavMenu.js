import React, { Component } from 'react';
import { Collapse, Container, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink } from 'reactstrap';
import { Link } from 'react-router-dom';
import { LoginMenu } from './api-authorization/LoginMenu';
import authService from './api-authorization/AuthorizeService';
import * as renderIf from 'render-if';
import './NavMenu.css';
import authActionDispatchers from '../Modules/Actions/AuthActionDispatchers';
import { connect } from 'react-redux';
import { authSelector } from '../Modules/Selectors/AuthSelectors';

class NavMenu extends Component {
  static displayName = NavMenu.name;

  constructor (props) {
    super(props);

    this.toggleNavbar = this.toggleNavbar.bind(this);
    this.state = {
      collapsed: true
    };
  }

  componentDidMount() {
    this._subscription = authService.subscribe(() => this.populateState());
    this.populateState();
  }

  async populateState() {
      const [isAuthenticated] = await Promise.all([authService.isAuthenticated()]);
      if (isAuthenticated) {
          authActionDispatchers.userLoginAction(isAuthenticated, await authService.getUser());
      } else {
          authActionDispatchers.userLogoutAction();
      }
  }

  toggleNavbar () {
    this.setState({
      collapsed: !this.state.collapsed
    });
  }

    render() {
        
    return (
      <header>
        <Navbar className="navbar-expand-sm navbar-toggleable-sm ng-white border-bottom box-shadow mb-3" light>
          <Container>
            <NavbarBrand tag={Link} to="/">Alumni Network Case</NavbarBrand>
            <NavbarToggler onClick={this.toggleNavbar} className="mr-2" />
            <Collapse className="d-sm-inline-flex flex-sm-row-reverse" isOpen={!this.state.collapsed} navbar>
              {renderIf(this.props.isAuthenticated)(() => (
                <ul className="navbar-nav flex-grow">
                  <NavItem>
                    <NavLink tag={Link} className="text-dark" to="/">Home</NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink tag={Link} className="text-dark" to={"/profile/" + this.props.userId}>Profile</NavLink>
                  </NavItem>
				          <NavItem>
                    <NavLink tag={Link} className="text-dark" to="/groups">Groups</NavLink>
				          </NavItem>
                  <NavItem>
                    <NavLink tag={Link} className="text-dark" to="/topics">Topics</NavLink>
                      </NavItem>
                  <NavItem>
                      <NavLink tag={Link} className="text-dark" to="/calendar">Calendar</NavLink>
                  </NavItem>
                  <LoginMenu>
                  </LoginMenu>
                </ul>
              ))}
              {renderIf(!this.props.isAuthenticated)(() => (
                <ul className="navbar-nav flex-grow">
                  <LoginMenu>
                  </LoginMenu>
                </ul>
              ))}
            </Collapse>
          </Container>
        </Navbar>
      </header>
    );
  }
}

export default connect(authSelector)(NavMenu);
