﻿import React, { Component } from 'react';
import * as renderIf from 'render-if';
import postActionDispatchers from '../../Modules/Actions/PostActionDispatchers';
import './CreateNewPost.styles.scss';
import TextArea from '../Assets/TextArea';
import { Input, Button } from 'reactstrap';

class CreateNewPost extends Component {
    constructor(props) {
        super(props);
        this.state = {
            post: {
                title: '', details: '', replyParentId: props.replyParentId
            },
            isCollapsed: true
        };
        this.formRef = React.createRef();
    }
    handleInputChange = e => {
        const { target } = e;
        const newPost = { ...this.state.post, [target.name]: target.value };
        this.setState({ post: newPost });
    }
    handleSubmit = e => {
        e.preventDefault();
        postActionDispatchers.postPostAction(this.state.post);
        this.setState({
            post: { ...this.state.post, title: '', details: '' },
        })
        this.handleCollapsible();
    }
    handleCollapsible = () => {
        const element = this.formRef.current;
        element.style.display === '' ? element.style.display = 'block' : element.style.display = '';
        this.setState({ isCollapsed: !this.state.isCollapsed });
    }
    render() {
        return (
            <div>
                {renderIf(this.state.isCollapsed)(() => (
                    <button onClick={this.handleCollapsible} className="newPostButton">{this.props.replyParentId ? 'reply' : 'New post'}</button>
                ))}
                {renderIf(!this.state.isCollapsed)(() => (
                    <div className="closeHeader">
                        <button onClick={this.handleCollapsible} className="close" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                ))}
                <form onSubmit={this.handleSubmit} className="formPost" ref={this.formRef}>
                    {renderIf(!this.props.replyParentId)(() => (
                        <input placeholder="Title..." name="title" value={this.state.title} onChange={this.handleInputChange} className="inputTitle" />
                    ))}
                    <Input type="textarea" placeholder="Message..." name="details" value={this.state.details} required onChange={this.handleInputChange}/>
                    <Button type="submit" color="primary" className="submitButton">Publish</Button>
                </form>
            </div>
        );
    }
}

export default CreateNewPost;