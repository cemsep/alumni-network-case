﻿import React, { Component } from 'react';
import * as moment from 'moment';
import CreateNewPost from './CreateNewPost';
import CreatePostModal from '../Modal/CreatePostModal';
import './Post.styles.scss';
import { history } from '../..';
import * as renderIf from 'render-if';
import PostOptionsButton from '../Assets/PostOptionsButton';
import EditPost from '../EditPost';
import ReactMarkdown from 'react-markdown';
import ProfileImage from '../Assets/ProfileImage';
import { connect } from 'react-redux';
import { postDetailsSelector } from '../../Modules/Selectors/PostSelectors';
import PostTarget from './PostTarget';
import 'github-markdown-css';

const defaultUserImage = require('../Assets/images/defaultUser.png');

class Post extends Component {
    constructor(props) {
        super(props);
        this.state = { isEditOpen: false }
    }

    toggleEdit = () => {
        this.setState({ isEditOpen: !this.state.isEditOpen });
    }

    searchHit(filterText) {
        const { post, targetUser, targetGroup, targetTopic, sender } = this.props;
        if (!filterText) {
            return true;
        }
        if (post.title.toLowerCase().includes(filterText) || post.details.toLowerCase().includes(filterText)) {
            return true;
        }
        if ((targetUser && targetUser.userName.toLowerCase().includes(filterText)) || (targetGroup && targetGroup.name.toLowerCase().includes(filterText)) || (targetTopic && targetTopic.name.toLowerCase().includes(filterText)) || (sender && sender.userName.toLowerCase().includes(filterText))) {
            return true;
        }
        return false;
    }

    render() {
        const { post } = this.props;
        return (
            <div>
                {renderIf(this.searchHit(this.props.filterText))(() => (
                    <div className="postBox">
                        <div className="postHeader">
                            <ProfileImage id={post.senderId} className="userImage" />
                            <div className="postHeaderContent">
                                <div className="postTopHeader">
                                    <h4 className="postUserName" onClick={() => history.push(`/profile/${post.senderId}`)}>{this.props.sender.userName}</h4>
                                    <PostTarget targetUser={this.props.targetUser} targetTopic={this.props.targetTopic} targetGroup={this.props.targetGroup} />
                                </div>
                                <div className="postCreatedOn">{moment.utc(post.createdOn).local().fromNow()}</div>
                            </div>
                            <PostOptionsButton post={post} onEdit={this.toggleEdit} />
                        </div>
                        <div className="postBody">
                            <h5 className="postTitle" onClick={() => history.push(`/post/${post.id}`)}>{post.title}</h5>
                            <div className="postContent markdown-body">
                                <ReactMarkdown source={post.details} />
                            </div>
                        </div>
                        <CreatePostModal show={this.state.isEditOpen} handleClose={this.toggleEdit} post={this.props.post} edit={true} />
                    </div>
                    ))}
            </div>
        );
    }
}

export default connect(postDetailsSelector)(Post);