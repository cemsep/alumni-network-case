﻿import React, { Component } from 'react';
import { connect } from 'react-redux';
import { postsSelector } from '../../Modules/Selectors/PostSelectors';
import * as renderIf from 'render-if';
import postActionDispatchers from '../../Modules/Actions/PostActionDispatchers';
import * as moment from 'moment';
import CreatePostModal from '../Modal/CreatePostModal';
import { Button } from 'reactstrap';
import Post from './Post';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCaretRight } from '@fortawesome/free-solid-svg-icons';
import './PostTarget.styles.scss';

class PostTarget extends Component {

    render() {
        return (
            <div className='postTargetContent'>
                {renderIf(this.props.targetUser)(() => (
                    <div className='postTarget'>
                        <FontAwesomeIcon icon={faCaretRight} className='postTargetIcon'/>
                        <a className='postTargetName' href={`/profile/${this.props.targetUser.userId}`}>{this.props.targetUser.userName}</a>
                    </div>
                ))}
                {renderIf(this.props.targetGroup)(() => (
                    <div className='postTarget'>
                        <FontAwesomeIcon icon={faCaretRight} className='postTargetIcon'/>
                        <a className='postTargetName' href={`/group/${this.props.targetGroup.id}`}>{this.props.targetGroup.name}</a>
                    </div>
                ))}
                {renderIf(this.props.targetTopic)(() => (
                    <div className='postTarget'>
                        <FontAwesomeIcon icon={faCaretRight} className='postTargetIcon'/>
                        <a className='postTargetName' href={`/topic/${this.props.targetTopic.id}`}>{this.props.targetTopic.name}</a>
                    </div>
                ))}
            </div>
        );
    }
}

export default PostTarget;