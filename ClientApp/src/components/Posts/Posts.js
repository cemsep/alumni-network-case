﻿import React, { Component } from 'react';
import { connect } from 'react-redux';
import { postsSelector } from '../../Modules/Selectors/PostSelectors';
import * as renderIf from 'render-if';
import postActionDispatchers from '../../Modules/Actions/PostActionDispatchers';
import * as moment from 'moment';
import CreatePostModal from '../Modal/CreatePostModal';
import { Button } from 'reactstrap';
import Post from './Post';

class Posts extends Component {

    state = { showModal: false }

    componentDidMount() {
        postActionDispatchers.getPostsAction();
    }

    handleClickEvent = (event) => {
        this.setState({ showModal: true });
    }

    handleCloseModal = () => {
        this.setState({ showModal: false });
    }

    render() {
        return (
            <div>
                {renderIf(this.props.isLoading)(() => (
                    <div>Loading posts...</div>
                ))}
                {renderIf(!this.props.isLoading)(() => (
                    <div>
                        {renderIf(this.props.error)(() => (
                            <div>
                                There was an error getting posts
                                <br />
                                {this.props.error.message}
                            </div>
                        ))}
                        <Button color="primary" onClick={this.handleClickEvent}>New Post</Button>
                        {renderIf(this.props.posts)(() => (
                            <div>
                                {this.props.posts.map(post =>
                                    <Post post={post} key={post.id} />
                                )}
                            </div>
                        ))}
                        <CreatePostModal show={this.state.showModal} handleClose={this.handleCloseModal} />
                    </div>
                ))}
            </div>
        );
    }
}

export default connect(postsSelector)(Posts);