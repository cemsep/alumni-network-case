import React, { Component } from 'react';
import Topic from '../Topics/Topic';

class TopicViewPage extends Component {
    render() {
        const id = parseInt(this.props.match.params.topicId);
        
        return (
            <Topic id={id} />
        );
    }
}

export default TopicViewPage;
