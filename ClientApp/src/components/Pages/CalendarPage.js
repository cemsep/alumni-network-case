﻿import React, { Component } from 'react';
import CalendarView from '../Calendar/CalendarView';

class CalendarPage extends Component {

    render() {
        return (
            <div>
                <CalendarView />
            </div>
        );
    }
}

export default CalendarPage;
