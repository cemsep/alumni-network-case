import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { profileSelector } from '../../Modules/Selectors/ProfileSelectors';
import profileActionDispatchers from '../../Modules/Actions/ProfileActionDispatchers';
import { defaultProfilePicture } from '../Assets/DefaultProfilePicture';
import authService from '../api-authorization/AuthorizeService';

class ProfileCheck extends Component {

    componentDidMount() {
      this._subscription = authService.subscribe(() => this.populateState());
      this.populateState();
      profileActionDispatchers.getProfileAction(this.props.userId);
    }

    componentDidUpdate(prevProps) {
      if (prevProps.error !== this.props.error) {
        this.checkUserProfile(this.props.userId);
      }
    }

    async populateState() {
      const [user] = await Promise.all([authService.getUser()])
      this.setState({
          user: user
      });
  }

    checkUserProfile(userId) {
      if (!this.props.profile) {
        this.createProfile(userId);
      }
    }

    createProfile(userId) {
      profileActionDispatchers.postProfileAction({ 
        userId: userId,
        userName: this.state.user.name,
        email: this.state.user.name,
        phoneNumber: null,
        firstName: null,
        lastName: null, 
        status: null, 
        bio: null, 
        funFact: null, 
        profilePicture: defaultProfilePicture
      });
      window.location.reload(false);
    }

    render() {
      return (
        <div></div>
      );
    }

}

export default withRouter(connect(profileSelector)(ProfileCheck));

