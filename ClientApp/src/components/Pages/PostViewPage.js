﻿import React, { Component } from 'react';
import PostView from '../Thread/PostView';

class PostViewPage extends Component {

    render() {
        const id = parseInt(this.props.match.params.id);
        return (
            <div>
                <PostView id={id} />
            </div>
        );
    }
}

export default PostViewPage
