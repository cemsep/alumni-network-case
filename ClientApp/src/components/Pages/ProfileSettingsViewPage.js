import React, { Component } from 'react';
import ProfileSettings from '../Profile/ProfileSettings';

export default class ProfileSettingsViewPage extends Component {

    render() {
        const id = this.props.match.params.id;
        return (
            <div>
                <ProfileSettings id={id} />
            </div>
        );
    }
}