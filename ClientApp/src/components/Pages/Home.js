import React, { Component } from 'react';
import Posts from '../Posts/Posts';
import Events from '../Events/Events';
import TimeLine from '../TimeLine/TimeLine';
import ProfileCheck from './ProfileCheck';
import { connect } from 'react-redux';
import { timeLineSelector } from '../../Modules/Selectors/TimeLineSelectors';

class Home extends Component {
    static displayName = Home.name;

    render() {
        return (
            <div>
                <ProfileCheck />
                <TimeLine />
            </div>
        );
    }
}

export default Home;
