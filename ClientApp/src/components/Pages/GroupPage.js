﻿import React, { Component } from 'react';
import Group from '../Groups/Group';

class GroupPage extends Component {
    render() {
        const id = parseInt(this.props.match.params.groupId);
        return (
            <Group id={id}/>
        );
    }
}

export default GroupPage;
