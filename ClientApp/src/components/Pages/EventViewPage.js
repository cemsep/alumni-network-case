﻿import React, { Component } from 'react';
import EventView from '../EventView/EventView';

class EventViewPage extends Component {

    render() {
        const id = parseInt(this.props.match.params.id);
        return (
            <div>
                <EventView id={id} />
            </div>
        );
    }
}

export default EventViewPage
