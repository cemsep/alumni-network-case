import React, { Component } from 'react';
import Profile from '../Profile/Profile';
import authService from '../api-authorization/AuthorizeService';

export default class ProfileViewPage extends Component {

    state = { isLoading: true };

    componentDidMount() {
        this._subscription = authService.subscribe(() => this.populateState());
        this.populateState();
    }

    async populateState() {
        const [user] = await Promise.all([authService.getUser()]);
        this.setState({ currentUserId: user && user.sub, isLoading: false });
    }
  
    render() {
        const id = this.props.match.params.id;
        const { currentUserId } = this.state;
        const { isLoading } = this.state;

        if (isLoading) {
            return (
                <div>Loading Profile...</div>
            )
        } else {
            return (
                <div>
                    <Profile id={id} currentUserId={currentUserId} />
                </div>
            );
        }
    }
}