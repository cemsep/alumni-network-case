﻿import React from 'react';
import { connect } from 'react-redux';
import { profileSelector } from '../../Modules/Selectors/ProfileSelectors';

const ProfileUsername = (props) => {
    return <div>{props.profile.userName}</div>
}

export default connect(profileSelector)(ProfileUsername);