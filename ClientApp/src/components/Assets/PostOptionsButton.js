﻿import React, { Component } from 'react';
import './PostOptionsButton.styles.scss';
import { Dropdown, DropdownItem, DropdownMenu, DropdownToggle } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEllipsisV } from '@fortawesome/free-solid-svg-icons';
import * as renderIf from 'render-if';
import { history } from '../..';
import { isAuthorOfPostSelector } from '../../Modules/Selectors/AuthSelectors';
import postActionDispatchers from '../../Modules/Actions/PostActionDispatchers';
import { connect } from 'react-redux';

class PostOptionsButton extends Component {
    constructor(props) {
        super(props);
        this.state = { isDropdownOpen: false }
    }

    toggleDropdown = () => {
        this.setState({ isDropdownOpen: !this.state.isDropdownOpen });
    }


    handleDelete = () => {
        if(this.props.postView) {
            if (window.confirm('Are you sure you wish to delete this post?')) {
                postActionDispatchers.deletePostAction(this.props.post.id);
                history.push('/');
            }  
        } else {
            window.confirm('Are you sure you wish to delete this post?') && postActionDispatchers.deletePostAction(this.props.post.id);
        }
        
    }

    render() {
        return (
            <Dropdown isOpen={this.state.isDropdownOpen} toggle={this.toggleDropdown} className="dropdown">
                <DropdownToggle tag="button" className="dropDownButton">
                    <FontAwesomeIcon icon={faEllipsisV} />
                </DropdownToggle>
                <DropdownMenu>
                    {renderIf(!this.props.postView)(() => (
                        <DropdownItem onClick={() => history.push(`/post/${this.props.post.id}`)}>Open Thread</DropdownItem>
                    ))}
                    {renderIf(this.props.isAuthor)(() => (
                        <div>
                            <DropdownItem onClick={this.props.onEdit}>Edit</DropdownItem>
                            <DropdownItem divider />
                            <DropdownItem onClick={this.handleDelete}>Delete</DropdownItem>
                        </div>
                    ))}
                </DropdownMenu>
            </Dropdown>
        );
    }
}

export default connect(isAuthorOfPostSelector)(PostOptionsButton);