﻿import React, { Component } from 'react';
import './PostOptionsButton.styles.scss';
import Select from 'react-select'
import * as renderIf from 'render-if';
import { history } from '../..';
import { connect } from 'react-redux';
import profileActionDispatchers from '../../Modules/Actions/ProfileActionDispatchers';
import './ProfileSelect.styles.scss';
import { profileSelectSelector } from '../../Modules/Selectors/ProfileSelectors';
import defaultProfilePicture from '../Assets/images/defaultUser.png';

class ProfileSelect extends Component {
    constructor(props) {
        super(props);
        this.state = { value: null };
    }

    componentDidMount() {
        profileActionDispatchers.getProfilesAction();
    }

    componentDidUpdate(prevProps) {
        if (prevProps !== this.props) {
            this.setState({ value: null });
        }
    }

    handleChange = (value) => {
        this.props.onChange(value);
        this.setState({ value: value });
    }

    render() {
        let options = [];
        if (this.props.profiles) {
            options = this.props.profiles.map(p => {
                return {
                    id: p.userId,
                    value: p.userName,
                    label: <div className='profileSelectOption'><img src={p.profilePicture ? `data:image/jpeg;base64,${p.profilePicture}` : defaultProfilePicture} alt="profile" className='profileSelectImg' /><div className='profileSeletUserName'>{p.userName}</div></div>
                }
            });
        }
        return (
            <Select options={options} isMulti={this.props.isMulti} value={this.state.value} onChange={this.handleChange}/>
        );
    }
}

export default connect(profileSelectSelector)(ProfileSelect);