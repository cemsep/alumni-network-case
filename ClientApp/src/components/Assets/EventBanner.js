import React from 'react';
import { connect } from 'react-redux';
import { eventSelector } from '../../Modules/Selectors/EventSelectors';
import './EventBanner.styles.scss';

const defaultEventImage = require('./images/default-event-thumb.png');

const EventBanner = (props) => {
    const className = props.event.bannerImg ? 'eventBannerImg' : 'eventDefaultImg';

    return <img src={props.event.bannerImg ? `data:image/jpeg;base64,${props.event.bannerImg}` : defaultEventImage} alt='banner.img' className={className} />
}

export default connect(eventSelector)(EventBanner);