﻿import React, { Component } from 'react';
import './PostOptionsButton.styles.scss';
import Select from 'react-select'
import * as renderIf from 'render-if';
import { connect } from 'react-redux';
import './ProfileSelect.styles.scss';
import topicActionDispatchers from '../../Modules/Actions/TopicActionDispatchers';
import { topicsSelector } from '../../Modules/Selectors/TopicSelectors';

class TopicSelect extends Component {
    constructor(props) {
        super(props);
        this.state = { value: null };
    }

    componentDidMount() {
        topicActionDispatchers.getTopicsAction();
    }

    componentDidUpdate(prevProps) {
        if (prevProps !== this.props) {
            this.setState({ value: null });
        }
    }

    handleChange = (value) => {
        this.props.onChange(value);
        this.setState({ value: value });
    }

    render() {
        let options = [];
        if (this.props.topics) {
            options = this.props.topics.map(t => {
                return {
                    id: t.id,
                    value: t.name,
                    label: <div className='profileSelectOption'><div className='profileSeletUserName'>{t.name}</div></div>
                }
            });
        }
        return (
            <div>
                {renderIf(this.props.isLoading && !this.props.topics)(() => (
                    <div>Loading...</div>
                ))}
                {renderIf(this.props.topics)(() => (
                    <Select options={options} value={this.state.value} onChange={this.handleChange} />
                ))}
            </div>
        );
    }
}

export default connect(topicsSelector)(TopicSelect);