﻿import React, { Component } from 'react';
import './PostOptionsButton.styles.scss';
import Select from 'react-select'
import * as renderIf from 'render-if';
import { connect } from 'react-redux';
import './ProfileSelect.styles.scss';
import groupActionDispatchers from '../../Modules/Actions/GroupActionDispatchers';
import { groupsSelector } from '../../Modules/Selectors/GroupSelectors';

class GroupSelect extends Component {
    constructor(props) {
        super(props);
        this.state = { value: null };
    }

    componentDidMount() {
        groupActionDispatchers.getGroupsAction();
    }

    componentDidUpdate(prevProps) {
        if (prevProps !== this.props) {
            this.setState({ value: null });
        }
    }

    handleChange = (value) => {
        this.props.onChange(value);
        this.setState({ value: value });
    }

    render() {
        let options = [];
        if (this.props.groups) {
            options = this.props.groups.map(g => {
                return {
                    id: g.id,
                    value: g.name,
                    label: <div className='profileSelectOption'><div className='profileSeletUserName'>{g.name}</div></div>
                }
            });
        }
        return (
            <div>
                {renderIf(this.props.isLoading && !this.props.groups)(() => (
                    <div>Loading...</div>
                ))}
                {renderIf(this.props.groups)(() => (
                    <Select options={options} value={this.state.value} onChange={this.handleChange} />
                ))}
            </div>
        );
    }
}

export default connect(groupsSelector)(GroupSelect);