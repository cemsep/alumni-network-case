﻿import React, { Component } from 'react';
import './TextArea.styles.scss';
import autosize from 'autosize';

class TextArea extends Component {
    constructor(props) {
        super(props);
        this.myRef = React.createRef();
    }

    componentDidMount() {
        autosize(this.myRef.current);
    }

    handleInputChange = e => {
        this.props.onChange(e);
    }
    render() {
    return (
        <textarea placeholder={this.props.placeholder} name={this.props.name} value={this.props.value} required={this.props.required} onChange={this.handleInputChange} className="textArea" ref={this.myRef} />
        );
    }
}

export default TextArea;