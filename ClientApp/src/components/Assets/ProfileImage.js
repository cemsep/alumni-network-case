﻿import React from 'react';
import { connect } from 'react-redux';
import { profileSelector } from '../../Modules/Selectors/ProfileSelectors';
import { defaultProfilePicture } from './DefaultProfilePicture';

const ProfileImage = (props) => {
    return <img className="userImage" src={props.profile.profilePicture ? `data:image/jpeg;base64,${props.profile.profilePicture}` : defaultProfilePicture} alt="UserImage" />
}

export default connect(profileSelector)(ProfileImage);