﻿import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as renderIf from 'render-if';
import * as moment from 'moment';
import { repliesSelector } from '../../Modules/Selectors/PostSelectors';
import postActionDispatchers from '../../Modules/Actions/PostActionDispatchers';
import './Reply.styles.scss';
import CreateNewPost from '../Posts/CreateNewPost';
import Replies from './Replies';
import PostOptionsButton from '../Assets/PostOptionsButton';
import { isAuthorOfPostSelector } from '../../Modules/Selectors/AuthSelectors';
import EditPost from '../EditPost';
import ReactMarkdown from 'react-markdown';
import 'github-markdown-css';

class Reply extends Component {
    constructor(props) {
        super(props);
        this.state = { isEditOpen: false }
    }

    componentDidMount() {
        postActionDispatchers.getRepliesAction(this.props.post.id);
    }

    toggleEdit = () => {
        this.setState({ isEditOpen: !this.state.isEditOpen });
    }

    render() {
        const { post } = this.props;
        return (
            <div>
                {renderIf(!post.deleted && !post.replies)(() => (
                    <div>
                        <div className="reply">
                            {renderIf(post.deleted)(() => (
                                <div>(Post deleted)</div>
                            ))}
                            {renderIf(!post.deleted)(() => (
                                <div>
                                    <div className="replyHeader">
                                        <div className="replyUserName">{this.props.sender ? this.props.sender.userName : 'Anonymous'}</div>
                                        <div className="replyCreatedOn">{moment.utc(post.createdOn).fromNow()}</div>
                                        {renderIf(!!post.lastUpdated)(() => (
                                            <div className="edited">(edited)</div>
                                        ))}
                                        <PostOptionsButton post={post} onEdit={this.toggleEdit} />
                                    </div>
                                    {renderIf(!this.state.isEditOpen)(() => (
                                        <div className="replyContent markdown-body">
                                            <ReactMarkdown source={post.details} />
                                        </div>
                                    ))}
                                    {renderIf(this.state.isEditOpen)(() => (
                                        <EditPost toggle={this.toggleEdit} post={post} />
                                    ))}
                                    <CreateNewPost replyParentId={post.id} />
                                </div>
                            ))}
                        </div>
                        {renderIf(this.props.replies)(() => (
                            <Replies replies={this.props.replies} />
                        ))}
                    </div>
                ))}
            </div>
        );
    }
}

export default connect(repliesSelector)(connect(isAuthorOfPostSelector)(Reply));