﻿import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as renderIf from 'render-if';
import * as moment from 'moment';
import { postSelector, threadSelector } from '../../Modules/Selectors/PostSelectors';
import postActionDispatchers from '../../Modules/Actions/PostActionDispatchers';
import Reply from './Reply';
import CreateNewPost from '../Posts/CreateNewPost';
import ReactMarkdown from 'react-markdown';
import PostOptionsButton from '../Assets/PostOptionsButton';
import CreatePostModal from '../Modal/CreatePostModal';
import { isAuthorOfPostSelector } from '../../Modules/Selectors/AuthSelectors';
import 'github-markdown-css';


class PostView extends Component {

    state = { isEditOpen: false }

    componentDidMount() {
        postActionDispatchers.getPostAction(this.props.id);
        postActionDispatchers.getRepliesAction(this.props.id);
    }

    toggleEdit = () => {
        this.setState({ isEditOpen: !this.state.isEditOpen });
    }

    render() {
        const { post } = this.props;
        return (
            <div>
                {renderIf(post)(() => (
                    <div>
                        <div className="threadHeader">
                            <h1>{post.title}</h1>
                            {renderIf(this.props.isAuthor)(() => (
                                <PostOptionsButton post={post} onEdit={this.toggleEdit} postView={true} />
                            ))}
                        </div>
                        <ReactMarkdown source={post.details} className='markdown-body'/>
                        <CreateNewPost replyParentId={post.id}/>
                        {renderIf(this.props.replies)(() => (
                            <div>
                                {this.props.replies.map(reply => (<Reply key={reply.id} post={reply}/>))}
                            </div>
                        ))}
                        <CreatePostModal show={this.state.isEditOpen} handleClose={this.toggleEdit} post={this.props.post} edit={true} />
                    </div>
                ))}
            </div>
        );
    }
}

export default connect(threadSelector)(connect(isAuthorOfPostSelector)(PostView));