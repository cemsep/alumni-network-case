﻿import React from 'react';
import Reply from './Reply';

const Replies = (props) => {
        return (
            <div className="replies">
                {props.replies.map(reply => (<Reply key={reply.id} post={reply} />))}
            </div>
        );
}

export default Replies;