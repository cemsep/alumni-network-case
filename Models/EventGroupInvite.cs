﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AlumniNetworkCase.Models
{
    public class EventGroupInvite
    {
        [Key]
        public int Id { get; set; }


        [ForeignKey("Event")]
        public int EventId { get; set; }
        public Event Event { get; set; }

        [ForeignKey("Group")]
        public int GroupId { get; set; }
        public Group Group { get; set; }

        public EventGroupInvite() { }

        public EventGroupInvite(Event theEvent, Group group)
        {
            this.Event = theEvent;
            this.Group = group;
        }
    }
}
