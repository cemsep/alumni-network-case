﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AlumniNetworkCase.Models
{
    public class EventTopicInvite
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Event")]
        public int EventId { get; set; }
        public Event Event { get; set; }

        [ForeignKey("Topic")]
        public int TopicId { get; set; }
        public Topic Topic { get; set; }

        public EventTopicInvite() { }

        public EventTopicInvite(Event theEvent, Topic topic)
        {
            this.Event = theEvent;
            this.Topic = topic;
        }

    }
}
