﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AlumniNetworkCase.Models
{
    public class RSVP
    {
        [Key]
        public int Id { get; set; }


        public DateTime LastUpdated { get; set; } = DateTime.UtcNow;
        public int GuestCount { get; set; }


        [ForeignKey("Profile")]
        public string ProfileId { get; set; }
        public Profile Profile { get; set; }

        [ForeignKey("Event")]
        public int EventId { get; set; }
        public Event Event { get; set; }


    }
}
