﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AlumniNetworkCase.Models
{
    public class EventMember
    {
        [ForeignKey("Event")]
        public int EventId { get; set; }
        public Event Event { get; set; }
        [ForeignKey("Profile")]
        public string ProfileId { get; set; }
        public Profile Profile { get; set; }

        public EventMember() { }

        public EventMember(int eventId, string profileId)
        {
            ProfileId = profileId;
            EventId = eventId;
        }
    }
}
