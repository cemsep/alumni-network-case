using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AlumniNetworkCase.Models
{
    public class Profile
    {
        [Required]
        [Key]
        [ForeignKey("User")]
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Status { get; set; }
        public string Bio { get; set; }
        public string FunFact { get; set; }
        public byte[] ProfilePicture { get; set; }

        public IEnumerable<GroupMember> GroupMembers { get; set; }
        public IEnumerable<EventUserInvite> EventUserInvites { get; set; }
        public IEnumerable<Event> Events { get; set; }
        [InverseProperty("Sender")]
        public IEnumerable<Post> PostsSent { get; set; }
        [InverseProperty("TargetUser")]
        public IEnumerable<Post> PostsTargetOf { get; set; }
        public IEnumerable<TopicMember> TopicMembers { get; set; }

        public Profile() : base() { }

        public Profile(string userId, string userName, string email, 
            string phoneNumber, string firstName, string lastName, 
            string status, string bio, string funFact, byte[] profilePicture)
        {
            UserId = userId;
            UserName = userName;
            Email = email;
            PhoneNumber = phoneNumber;
            FirstName = firstName;
            LastName = lastName;
            Status = status;
            Bio = bio;
            FunFact = funFact;
            ProfilePicture = profilePicture;
        }

        public Profile EditProfile(Profile profile)
        {
            FirstName = profile.FirstName;
            LastName = profile.LastName;
            Status = profile.Status;
            Bio = profile.Bio;
            FunFact = profile.FunFact;
            ProfilePicture = profile.ProfilePicture;
            return this;
        }
    }
}
