﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AlumniNetworkCase.Models
{
    public class Post
    {
        [Key]
        public int Id { get; set; }
        public string Title { get; set; }
        public string Details { get; set; }


        [ForeignKey("ReplyParent")]
        public int? ReplyParentId { get; set; }
        public Post? ReplyParent { get; set; }


        public DateTime CreatedOn { get; set; } = DateTime.UtcNow;
        public DateTime? LastUpdated { get; set; }


        [ForeignKey("Sender")]
        public string SenderId { get; set; }
        public Profile Sender { get; set; }

        [ForeignKey("TargetUser")]
        public string? TargetUserId { get; set; }
        public Profile? TargetUser { get; set; }


        [ForeignKey("TargetGroup")]
        public int? TargetGroupId { get; set; }
        public Group? TargetGroup { get; set; }

        [ForeignKey("TargetTopic")]
        public int? TargetTopicId { get; set; }
        public Topic? TargetTopic { get; set; }

        public bool Deleted { get; set; } = false;

        public Post() { }

        public Post(string title, string details, Profile sender, Group targetgroup, Topic targettopic)
        {
            this.Title = title;
            this.Details = details;
            this.Sender = sender;
            this.TargetGroup = targetgroup;
            this.TargetTopic = targettopic;
        }

        public Post EditPost(Post post)
        {
            Title = post.Title;
            Details = post.Details;
            LastUpdated = DateTime.UtcNow;
            return this;
        }

        public Post DeletePost()
        {
            Title = null;
            Details = null;
            SenderId = null;
            Deleted = true;
            return this;
        }
        
    }
}
