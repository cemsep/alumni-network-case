﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AlumniNetworkCase.Models
{
    public class ApplicationUser : IdentityUser
    {
        public Profile Profile { get; set; }

        public ApplicationUser() : base() { }

        public ApplicationUser(string username, string pwd, string email) : base(username)
        {
            this.PasswordHash = pwd;
            this.Email = email;
        }
    }
}
