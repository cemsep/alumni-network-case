﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AlumniNetworkCase.Models
{
    public class Event
    {
        [Key]
        public int Id { get; set; }

        public DateTime LastUpdated { get; set; } = DateTime.UtcNow;
        public string Name { get; set; }
        public string Description { get; set; }
        public bool AllowGuests { get; set; } = true;


        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }


        [ForeignKey("CreatedBy")]
        public string CreatedById { get; set; }
        public Profile CreatedBy { get; set; }


        public byte[] BannerImg { get; set; }

        public IEnumerable<EventMember> EventMembers { get; set; }

        public Event() { }

        public Event(string name, string description, DateTime start, DateTime end, byte[] bannerImg,Profile creator, bool allowguests = true)
        {
            this.Name = name;
            this.Description = description;
            this.AllowGuests = allowguests;
            this.StartTime = start;
            this.EndTime = end;
            this.BannerImg = bannerImg;
            this.CreatedBy = creator;
        }
    }
}
