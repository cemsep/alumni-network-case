﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AlumniNetworkCase.Models
{
    public class Topic
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public IEnumerable<Post> Posts { get; set; }
        public IEnumerable<EventTopicInvite> EventTopicInvite { get; set; }
        public IEnumerable<TopicMember> TopicMember { get; set; }

        public Topic() { }

        public Topic(string name, string description)
        {
            this.Name = name;
            this.Description = description;
        }
    }
}
