﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AlumniNetworkCase.Models
{
    public class TopicMember
    {
        public int TopicId { get; set; }
        public Topic Topic { get; set; }
        public string ProfileId {get;set;}
        public Profile Profile {get;set;}

        public TopicMember() { }

        public TopicMember(Profile profile, Topic topic)
        {
            this.Profile = profile;
            this.Topic = topic;
        }

        public TopicMember(int topicId, string profileId)
        {
            TopicId = topicId;
            ProfileId = profileId;
        }
    }
}
