﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AlumniNetworkCase.Models
{
    public class GroupMember
    {
        public int GroupId { get; set; }
        public Group Group { get; set; }
        public string ProfileId { get; set; }
        public Profile Profile { get; set; }

        public GroupMember() { }

        public GroupMember(Profile profile, Group group)
        {
            this.Profile = profile;
            this.Group = group;
        }

        public GroupMember(int groupId, string profileId)
        {
            ProfileId = profileId;
            GroupId = groupId;
        }
    }
}
