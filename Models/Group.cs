﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AlumniNetworkCase.Models
{
    public class Group
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsPrivate { get; set; } = false;

        public IEnumerable<GroupMember> GroupMembers { get; set; }
        public IEnumerable<EventGroupInvite> EventGroupInvites { get; set; }
        public IEnumerable<Post> Posts { get; set; }

        public Group() { }

        public Group(string name, string description, bool isprivate = false)
        {
            this.Name = name;
            this.Description = description;
            this.IsPrivate = isprivate;
        }
    }
}
