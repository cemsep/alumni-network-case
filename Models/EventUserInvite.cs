﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AlumniNetworkCase.Models
{
    public class EventUserInvite
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Event")]
        public int EventId { get; set; }
        public Event Event { get; set; }

        [ForeignKey("Profile")]
        public string ProfileId { get; set; }
        public Profile Profile { get; set; }

        public EventUserInvite() { }

        public EventUserInvite(Event theEvent, Profile profile)
        {
            this.Event = theEvent;
            this.Profile = profile;
        }
    }
}
