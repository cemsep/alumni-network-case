# AlumniNetworkCase

.NET Core Web Application (React Front)

A website with authentication, posts++


## Getting Started

For the deployment we're using the .NET Core SDK which you can obtain it here:
https://docs.microsoft.com/en-us/dotnet/core/install/sdk?pivots=os-windows

Due to known bugs with MS Azure and oversubscription due to COVID-19, it may not be
possible to successfully deploy an application to Azure using normal methods. Therefore
we will use a simple reverse proxy called [Ngrok](https://ngrok.com/).

Sign up to the website for a free account to get an access token and follow the
installation instructions to install the program locally. Configure the local application
with the following command:

```
./ngrok authtoken <YOUR_AUTHTOKEN>
```

We will also use a [Docker](https://docs.docker.com/install/) image and commands for our deployment. 


After setting up the .NET Core SDK and Ngrok (and Docker), you can clone the project to a local directory.

```
git init
git clone git@gitlab.com:cemsep/alumni-network-case.git
```

Open the solution in Visual Studio or Visual Studio Code

***Note: If using Visual Studio Code, remember to download necessarily extensions such as C#***


In Command Prompt or Terminal run the command in the project root, assuming that you are running Docker on your current machine
and make sure you're using linux containers:

```
docker-compose up
```

Let this command run in your terminal (it might take a while to finish), and in a seperate terminal run the Ngrok command:

```
./ngrok http 8000
```

This will produce a status screen like this one:

<img src="./docs/ngrok.png" alt="env config" width="800"/>

This screen shows the addresses at which the application has been made available and
the address of the local web interface for managing the tunnel. The forwarded addresses
are regenerated every time the app is restarted so try not to close the app too much. This
screen also shows the incoming HTTP requests and connection statistics happening in
real time.
Once the tunnel is configured an operational, you can try browsing to the HTTPS
address in your browser to see what your application might look like once deployed. In
the above case that URL is:

```
https://704b5052.ngrok.io
```

Ngrok can be applied in a similar way to any kind of web app; the only requirement is
that the app is currently serving on a local port and you leave the tunnel process
running.

***Due to Ngrok deployment, the IP Address of the deployed app will change each time we run the ngrok command.
Which means that the redirect URL for the Identity Service Providers (such as Google, Microsoft) needs to be 
changed as well. In order for that to work, you can set up your own ClientId & ClientSecret for each provider in
the appsettings.json, providing the redirect URL that Ngrok generates. This issue will be fixed once the Azure services
gets back on running.***


### Prerequisites

.NET Core SDK

React

Visual Studio 2017/19 OR Visual Studio Code

Docker

Ngrok


## Authors

* Jens CT Hetland |   [Hetland](https://gitlab.com/Hetland)
* Jonas E Horvli  |   [Horvli](https://gitlab.com/jonas.ellefsen.horvli)
* Cem Pedersen    |   [Pedersen](https://gitlab.com/cemsep)