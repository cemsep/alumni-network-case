﻿using AlumniNetworkCase.Data;
using AlumniNetworkCase.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;

namespace AlumniNetworkCase
{
    public class SeedDB
    {
        private int numOfSamples = 5;

        static string[] bios =
        {
            "Very outgoing guy, full of life, love cake, motorcyclist",
            "Social and fond of parties, member of a secret society",
            "Fun to hang around, likes to repair stuff",
            "Award-winning chef, have traveled to lots of countries",
            "Daughter of Alex Vanderbilt (the real estate billionaire), loves to fish",
        };

        ApplicationUser[] applicationUsers =
        {
            new ApplicationUser("OogaChakka", "122rfputt", "123life@hotmail.com"),
            new ApplicationUser("AlistairCrowley", "vwf2rtig0y", "homebase@hell.no"),
            new ApplicationUser("BobBob", "vnipriyud888", "bob.bob123@gmail.com"),
            new ApplicationUser("VanessaHagen", "1dpcoucr909", "vanessa.hagen@spritelysome.com"),
            new ApplicationUser("AliceVanderbilt", "aazp087vhnvnklowkmn", "daughter@vanderbilt.com"),
        };

        public SeedDB(ApplicationDbContext dbContext)
        {
            Console.WriteLine("Seeding data...");
            // If db doesn't have enough users for seeding of events, groups etc, then seed
            if (dbContext.Users.Count() < numOfSamples)
            {
                dbContext.Users.AddRange(applicationUsers);
                dbContext.SaveChanges();
            }
            if (dbContext.Profiles.Count() < numOfSamples) { SeedProfiles(dbContext); }
            if (dbContext.Groups.Count() < numOfSamples) { SeedGroups(dbContext); }
            if (dbContext.Topics.Count() < numOfSamples) { SeedTopics(dbContext); }
            if (dbContext.Posts.Count() < numOfSamples) { SeedPosts(dbContext); }
            if (dbContext.Events.Count() < numOfSamples) { SeedEvents(dbContext); }
            
            if (dbContext.GroupMembers.Count() < numOfSamples       ||
                dbContext.TopicMembers.Count() < numOfSamples       ||
                dbContext.EventGroupInvites.Count() < numOfSamples  ||
                dbContext.EventTopicInvites.Count() < numOfSamples  ||
                dbContext.EventUserInvites.Count() < numOfSamples) 
                    { 
                        SeedIntermediates(dbContext); 
                    }

        }

        private void SeedProfiles(ApplicationDbContext dbContext) 
        {
            Profile[] profiles = 
            {
                new Profile(dbContext.Users.Where(u => u.UserName == "OogaChakka").First().Id, 
                    dbContext.Users.Where(u => u.UserName == "OogaChakka").First().UserName,
                    dbContext.Users.Where(u => u.UserName == "OogaChakka").First().Email, 
                    dbContext.Users.Where(u => u.UserName == "OogaChakka").First().PhoneNumber,  
                    "Ooga", "Chakka", null, bios[0], "Can make almost anything from anything", null),
                new Profile(dbContext.Users.Where(u => u.UserName == "AlistairCrowley").First().Id, 
                    dbContext.Users.Where(u => u.UserName == "AlistairCrowley").First().UserName,
                    dbContext.Users.Where(u => u.UserName == "AlistairCrowley").First().Email,
                    dbContext.Users.Where(u => u.UserName == "AlistairCrowley").First().PhoneNumber,   
                    "Alistair", "Crowley", null, bios[1], "Can spontaneously combust", null),
                new Profile(dbContext.Users.Where(u => u.UserName == "BobBob").First().Id, 
                    dbContext.Users.Where(u => u.UserName == "BobBob").First().UserName,
                    dbContext.Users.Where(u => u.UserName == "BobBob").First().Email,
                    dbContext.Users.Where(u => u.UserName == "BobBob").First().PhoneNumber,   
                    "Bob", "Bob", null, bios[2], "Can literally jump to the skies", null),
                new Profile(dbContext.Users.Where(u => u.UserName == "VanessaHagen").First().Id, 
                    dbContext.Users.Where(u => u.UserName == "VanessaHagen").First().UserName,
                    dbContext.Users.Where(u => u.UserName == "VanessaHagen").First().Email,
                    dbContext.Users.Where(u => u.UserName == "VanessaHagen").First().PhoneNumber,   
                    "Vanessa", "Hagen", null, bios[3], "Can turn any argument into a foodfight", null),
                new Profile(dbContext.Users.Where(u => u.UserName == "AliceVanderbilt").First().Id, 
                    dbContext.Users.Where(u => u.UserName == "AliceVanderbilt").First().UserName,
                    dbContext.Users.Where(u => u.UserName == "AliceVanderbilt").First().Email,
                    dbContext.Users.Where(u => u.UserName == "AliceVanderbilt").First().PhoneNumber,   
                    "Alice", "Vanderbilt", null, bios[4], "Literally owns half of upstate New York", null)
            };

            dbContext.Profiles.AddRange(profiles);
            dbContext.SaveChanges();
        }

        private void SeedEvents(ApplicationDbContext dbContext)
        {
            DateTime[] startTimes =
            {
                new DateTime(2020, 5, 1, 15, 30, 0),
                new DateTime(2020, 12, 22, 15, 30, 0),
                new DateTime(2020, 5, 4, 15, 30, 0),
                new DateTime(2020, 8, 21, 15, 30, 0),
                new DateTime(2020, 2, 19, 15, 30, 0)
            };
            DateTime[] endTimes =
            {
                new DateTime(2040, 6, 5, 15, 30, 0),
                new DateTime(2040, 6, 5, 15, 30, 0),
                new DateTime(2040, 6, 5, 15, 30, 0),
                new DateTime(2040, 6, 5, 15, 30, 0),
                new DateTime(2040, 6, 5, 15, 30, 0),
            };
            string[] names =
            {
                "Festivities for summer break",
                "Getting ready for holidays",
                "Making lasagna",
                "Hand in pictures",
                "Create vase"
            };
            string[] descriptions =
            {
                "Remember to bring your own food and drinks!",
                "We meet up at the square",
                "Our kitchen, with extra spicy ingredients",
                "The newly made ones, not the ones in the garage",
                "Try to make it like the last time, but with smoother edges"
            };

            var profilesFromDb = dbContext.Profiles.ToList();

            Event[] events = new Event[numOfSamples];
            for (int eventNum = 0; eventNum < numOfSamples; eventNum++)
            {
                events[eventNum] = new Event(names[eventNum], 
                                        descriptions[eventNum], 
                                        startTimes[eventNum], 
                                        endTimes[eventNum], 
                                        null,
                                        profilesFromDb[eventNum]);
            }
            dbContext.Events.AddRange(events);
            dbContext.SaveChanges();
        }

        private void SeedGroups(ApplicationDbContext dbContext)
        {
            string[] names = 
            {
                "Those who love backpacking",
                "Climber's heaven",
                "Get news before they're in the newspapers!",
                "For those who love cooking healthy meals <3",
                "Group for homeowners in America"
            };

            string[] descriptions =
            {
                "All across Europe, but with most of our members in Germany",
                "For everyone who loves climbing, with regular events for gathering",
                "Be the first to get scoops on new events",
                "Be it regular people or chefs by trade, everyone is welcome here!",
                ""
            };

            Group[] groups = new Group[numOfSamples];
            for (int groupNum = 0; groupNum < numOfSamples; groupNum++)
            {
                groups[groupNum] = new Group(names[groupNum], 
                                        descriptions[groupNum]);
            }
            dbContext.Groups.AddRange(groups);
            dbContext.SaveChanges();
        }

        private void SeedTopics(ApplicationDbContext dbContext)
        {
            string[] names =
            {
                "Backpacking across Europe",
                "Bouldering",
                "Tips and tricks",
                "Vegetarian meals",
                "Getting the best deals on plots"
            };

            string[] descriptions =
            {
                "Find the best places to travel, and more!",
                "When talking about indoor climbing",
                "Tips and tricks for how to get better scoops etc",
                "Learn to make vegetarian meals like those at Google",
                "How to see which plots are undervalued, pricing based on location etc"
            };

            Topic[] topics = new Topic[numOfSamples];
            for (int topicNum = 0; topicNum < numOfSamples; topicNum++)
            {
                topics[topicNum] = new Topic(names[topicNum], 
                                        descriptions[topicNum]);
            }
            dbContext.Topics.AddRange(topics);
            dbContext.SaveChanges();
        }

        private void SeedPosts(ApplicationDbContext dbContext)
        {
            string[] titles =
            {
                "On backpacking in Norway",
                "Solution for climbing one of the red routes at Bergen klatresenter",
                "I have a tip, always have friends in places where there's likely to be news",
                "How to make olives stuffed with feta cheese",
                "Proximity to city centres"
            };

            string[] details =
            {
                "I could imagine it would be quite cold, at least in the winter. is there anyone here that has tried this?",
                "I tried one of the more difficult routes at Bergen klatresenter the other day, the red one in the left corner. Has anyone else tried it?",
                "So I'm very social, and as such I have a lot of friends everywhere around the world. This has helped me greatly with getting scoops before anyone else.",
                "So I saw some olives stuffed with feta cheese in the store the other day, and decided to make them at home. Anyone know where to start?",
                "The price of plots should go up greatly as you move closer to the city centres. Don't worry, you will still get lots of offers on the house you build."
            };

            var profilesFromDb = dbContext.Profiles.ToList();
            var topicsFromDb = dbContext.Topics.ToList();
            var groupsFromDb = dbContext.Groups.ToList();

            Post[] posts = new Post[numOfSamples];
            for (int postNum = 0; postNum < numOfSamples; postNum++)
            {
                posts[postNum] = new Post(  titles[postNum], 
                                        details[postNum], 
                                        profilesFromDb[postNum], 
                                        groupsFromDb[postNum], 
                                        topicsFromDb[postNum]);
            }
            dbContext.Posts.AddRange(posts);
            dbContext.SaveChanges();
        }

        private void SeedIntermediates(ApplicationDbContext dbContext)
        {
            GroupMember[] groupMembers = new GroupMember[numOfSamples];
            TopicMember[] topicMembers = new TopicMember[numOfSamples];

            EventGroupInvite[] eventGroupInvites = new EventGroupInvite[numOfSamples];
            EventTopicInvite[] eventTopicInvites = new EventTopicInvite[numOfSamples];
            EventUserInvite[] eventUserInvites = new EventUserInvite[numOfSamples];
            // TODO: RSVP?

            var profiles = dbContext.Profiles.ToList();
            var groups = dbContext.Groups.ToList();
            var topics = dbContext.Topics.ToList();
            var events = dbContext.Events.ToList();

            if (dbContext.GroupMembers.Count() < numOfSamples)
            {
                for (int groupMemberNum = 0; groupMemberNum < numOfSamples; groupMemberNum++)
                {
                    groupMembers[groupMemberNum] = new GroupMember(profiles[groupMemberNum], groups[groupMemberNum]);
                }
                dbContext.AddRange(groupMembers);
                dbContext.SaveChanges();
            }

            if (dbContext.TopicMembers.Count() < numOfSamples)
            {
                for (int topicMemberNum = 0; topicMemberNum < numOfSamples; topicMemberNum++)
                {
                    topicMembers[topicMemberNum] = new TopicMember(profiles[topicMemberNum], topics[topicMemberNum]);
                }
                dbContext.AddRange(topicMembers);
                dbContext.SaveChanges();
            }

            if (dbContext.EventGroupInvites.Count() < numOfSamples)
            {
                for (int egiNum = 0; egiNum < numOfSamples; egiNum++)
                {
                    eventGroupInvites[egiNum] = new EventGroupInvite(events[egiNum], groups[egiNum]);
                }
                dbContext.AddRange(eventGroupInvites);
                dbContext.SaveChanges();
            }

            if (dbContext.EventTopicInvites.Count() < numOfSamples)
            {
                for (int etiNum = 0; etiNum < numOfSamples; etiNum++)
                {
                    eventTopicInvites[etiNum] = new EventTopicInvite(events[etiNum], topics[etiNum]);
                }
                dbContext.AddRange(eventTopicInvites);
                dbContext.SaveChanges();
            }

            if (dbContext.EventUserInvites.Count() < numOfSamples)
            {
                for (int euiNum = 0; euiNum < numOfSamples; euiNum++)
                {
                    eventUserInvites[euiNum] = new EventUserInvite(events[euiNum], profiles[euiNum]);
                }
                dbContext.AddRange(eventUserInvites);
                dbContext.SaveChanges();
            }

        }
    }
}
