﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AlumniNetworkCase.Data;
using AlumniNetworkCase.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;

namespace AlumniNetworkCase.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class PostController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public PostController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: api/Post
        [HttpGet]
        public async Task<IEnumerable<Post>> GetPosts(int? replyParentId)
        {
            string userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            if (replyParentId != null)
            {
                return await _context.Posts.Where(p => p.ReplyParentId == replyParentId).Include(p => p.Sender).OrderByDescending(p => p.CreatedOn).ToListAsync();
            }
            IEnumerable<Post> topicPosts = await _context.Posts.Where(p => p.ReplyParentId == null && p.Deleted == false && p.TargetTopic.TopicMember.FirstOrDefault(tm => tm.ProfileId == userId) != null).Include(p => p.TargetTopic).Include(p => p.Sender).ToListAsync();
            IEnumerable<Post> groupPosts = await _context.Posts.Where(p => p.ReplyParentId == null && p.Deleted == false && p.TargetGroup.GroupMembers.FirstOrDefault(gm => gm.ProfileId == userId) != null).Include(p => p.TargetGroup).Include(p => p.Sender).ToListAsync();
            IEnumerable<Post> privatePosts = await _context.Posts.Where(p => p.ReplyParentId == null && p.Deleted == false && (p.TargetUserId == userId || p.SenderId == userId)).Include(p => p.Sender).ToListAsync();
            IEnumerable<Post> publicPosts = await _context.Posts.Where(p => p.ReplyParentId == null && p.Deleted == false && p.SenderId != userId && p.TargetTopicId == null && p.TargetGroupId == null && p.TargetUserId == null).Include(p => p.Sender).ToListAsync();
            IEnumerable<Post> mixedPosts = publicPosts.Concat(privatePosts).Concat(topicPosts).Concat(groupPosts).OrderByDescending(p => p.CreatedOn);
            return mixedPosts;
        }

        // GET: api/Post/user
        [HttpGet("user")]
        public async Task<ActionResult<IEnumerable<Post>>> GetUserPosts() 
        {
            string userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            
            return await _context.Posts.Where(p => p.TargetUserId == userId).Include(p => p.Sender).OrderByDescending(p => p.CreatedOn).ToListAsync();
        }

        // GET: api/Post/user/id
        [HttpGet("user/{id}")]
        public async Task<ActionResult<IEnumerable<Post>>> GetUserPosts(string id) 
        {
            string userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            
            return await _context.Posts.Where(p => p.TargetUserId == userId && p.SenderId == id).Include(p => p.Sender).OrderByDescending(p => p.CreatedOn).ToListAsync();
        }

        // GET: api/Post/group/id
        [HttpGet("group/{id}")]
        public async Task<ActionResult<IEnumerable<Post>>> GetGroupPosts(int id) 
        {
            return await _context.Posts.Where(p => p.TargetGroupId == id).Include(p => p.Sender).OrderByDescending(p => p.CreatedOn).ToListAsync();
        }

        // GET: api/Post/topic/id
        [HttpGet("topic/{id}")]
        public async Task<ActionResult<IEnumerable<Post>>> GetTopicPosts(int id) 
        {
            return await _context.Posts.Where(p => p.TargetTopicId == id).Include(p => p.Sender).OrderByDescending(p => p.CreatedOn).ToListAsync();
        }

        // GET: api/Post/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Post>> GetPost(int id)
        {
            var posts = await _context.Posts.FindAsync(id);

            if (posts == null)
            {
                return NotFound();
            }

            return posts;
        }

        // PUT: api/Post/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<ActionResult<Post>> PutPost(int id, Post post)
        {
            string userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            Post originalPost = await _context.Posts.FindAsync(id);
            if (id != post.Id)
            {
                return BadRequest();
            }
            else if (!originalPost.SenderId.Equals(userId))
            {
                return Forbid();
            }
            Post updatedPost = originalPost.EditPost(post);

            _context.Entry(updatedPost).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PostExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return updatedPost;
        }

        // POST: api/Post
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Post>> PostPost(Post post)
        {
            string userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            post.SenderId = userId;
            _context.Posts.Add(post);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPosts", new { id = post.Id }, post);
        }

        // DELETE: api/Post/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Post>> DeletePost(int id)
        {
            var post = await _context.Posts.FindAsync(id);
            string userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            Post originalPost = await _context.Posts.FindAsync(id);
            if (post == null)
            {
                return NotFound();
            }
            else if (!originalPost.SenderId.Equals(userId))
            {
                return Forbid();
            }
            Post deletedPost = originalPost.DeletePost();

            _context.Entry(deletedPost).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PostExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return deletedPost;
        }

        private bool PostExists(int id)
        {
            return _context.Posts.Any(e => e.Id == id);
        }
    }
}
