﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AlumniNetworkCase.Data;
using AlumniNetworkCase.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;

namespace AlumniNetworkCase.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class TopicController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public TopicController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Topics
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Topic>>> GetTopics()
        {
            return await _context.Topics.Include(t => t.TopicMember).ThenInclude(tm => tm.Profile).ToListAsync();
        }

        // GET: api/Topics/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Topic>> GetTopic(int id)
        {
            var topic = await _context.Topics
                .Where(t => t.Id == id)
                .Include(t => t.Posts)
                .ThenInclude(p => p.Sender)
                .Include(t => t.TopicMember)
                .ThenInclude(tm => tm.Profile)
                .Include("EventTopicInvite.Event")
                .FirstOrDefaultAsync();

            if (topic == null)
            {
                return NotFound();
            }

            return topic;
        }

        // PUT: api/Topics/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTopic(int id, Topic topic)
        {
            if (id != topic.Id)
            {
                return BadRequest();
            }

            _context.Entry(topic).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TopicExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Topics
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Topic>> PostTopic(Topic topic)
        {
            _context.Topics.Add(topic);
            await _context.SaveChangesAsync();
            string userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            _context.TopicMembers.Add(new TopicMember(topic.Id, userId));
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTopic", new { id = topic.Id }, topic);
        }

        // DELETE: api/Topics/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Topic>> DeleteTopic(int id)
        {
            var topic = await _context.Topics.FindAsync(id);
            if (topic == null)
            {
                return NotFound();
            }

            _context.Topics.Remove(topic);
            await _context.SaveChangesAsync();

            return topic;
        }

        // POST: api/{id}/join
        [HttpPost("{topicId}/join")]
        public async Task<ActionResult<Topic>> JoinTopic(int topicId, string? userId)
        {
            var topic = await _context.Topics.FindAsync(topicId);
            string currentUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            string newMemberId = userId;

            if (topic == null || currentUserId == null)
            {
                return NotFound();
            }

            if (newMemberId == null)
            {
                newMemberId = currentUserId;
            }

            var topicMember = await _context.TopicMembers.FindAsync(topicId, newMemberId);
            if (topicMember != null)
            {
                return Conflict("User is already a member of this topic.");
            }

            _context.TopicMembers.Add(new TopicMember(topicId, newMemberId));
            await _context.SaveChangesAsync();

            return await _context.Topics.Where(t => t.Id == topicId).Include(t => t.Posts).Include(t => t.TopicMember).FirstOrDefaultAsync();
        }

        // DELETE: api/{id}/leave
        [HttpDelete("{topicId}/leave")]
        public async Task<ActionResult<Topic>> LeaveTopic(int topicId)
        {
            var topic = await _context.Topics.FindAsync(topicId);
            string currentUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            if (topic == null || currentUserId == null)
            {
                return NotFound();
            }

            var topicMember = await _context.TopicMembers.FindAsync(topicId, currentUserId);
            if (topicMember == null)
            {
                return Conflict("User is not a member of this topic.");
            }

            _context.TopicMembers.Remove(topicMember);
            await _context.SaveChangesAsync();

            return await _context.Topics.Where(t => t.Id == topicId).Include(t => t.Posts).Include(t => t.TopicMember).FirstOrDefaultAsync();
        }

        private bool TopicExists(int id)
        {
            return _context.Topics.Any(e => e.Id == id);
        }
    }
}
