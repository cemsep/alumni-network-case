﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AlumniNetworkCase.Data;
using AlumniNetworkCase.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;

namespace AlumniNetworkCase.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class EventController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public EventController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
            _context = context;
        }

        // GET: api/Events
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Event>>> GetEvents()
        {
            return await _context.Events
                .Include(e => e.EventMembers)
                .ThenInclude(em => em.Profile)
                .OrderBy(e => e.StartTime).ToListAsync();
        }

        // GET: api/Events/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Event>> GetEvent(int id)
        {
            var @event = await _context.Events
                .Include(e => e.EventMembers)
                .ThenInclude(em => em.Profile)
                .FirstOrDefaultAsync(e => id == e.Id);

            if (@event == null)
            {
                return NotFound();
            }

            return @event;
        }

        // PUT: api/Events/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutEvent(int id, Event @event)
        {
            if (id != @event.Id)
            {
                return BadRequest();
            }

            _context.Entry(@event).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EventExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Events
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Event>> PostEvent(Event @event)
        {
            string userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            @event.CreatedById = userId;
            _context.Events.Add(@event);
            await _context.SaveChangesAsync();

            _context.EventMembers.Add(new EventMember(@event.Id, userId));
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetEvent", new { id = @event.Id }, @event);
        }

        // DELETE: api/Events/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Event>> DeleteEvent(int id)
        {
            var @event = await _context.Events.FindAsync(id);
            if (@event == null)
            {
                return NotFound();
            }

            _context.Events.Remove(@event);
            await _context.SaveChangesAsync();

            return @event;
        }

        // POST: api/Event/{id}/join
        [HttpPost("{eventId}/join")]
        public async Task<ActionResult<Event>> JoinEvent(int eventId, string[] @newMembers)
        {
            var @event = await _context.Events.FindAsync(eventId);
            string currentUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            bool isMember = (await _context.EventMembers.FindAsync(eventId, currentUserId)) != null;
            if (@event == null)
            {
                return NotFound();
            }

            if (@newMembers.Length == 0)
            {
                if (!@event.AllowGuests)
                {
                    return Forbid();
                } else if (isMember)
                {
                    return Conflict("User is already a member of this event.");
                }

                _context.EventMembers.Add(new EventMember(eventId, currentUserId));
            } else
            {
                if (!isMember || !@event.CreatedById.Equals(currentUserId))
                {
                    return Forbid();
                }

                for (int i = 0; i < @newMembers.Length; i++)
                {
                    string newMemberId = @newMembers[i];
                    var @groupMember = await _context.EventMembers.FindAsync(eventId, newMemberId);
                    if (@groupMember != null)
                    {
                        return Conflict("User is already a member of this event.");
                    }
                    _context.EventMembers.Add(new EventMember(eventId, newMemberId));
                }
            }
            await _context.SaveChangesAsync();
            return await _context.Events.Where(e => e.Id == eventId).Include(e => e.EventMembers).FirstOrDefaultAsync();

        }

        private bool EventExists(int id)
        {
            return _context.Events.Any(e => e.Id == id);
        }
    }
}
