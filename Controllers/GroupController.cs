﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AlumniNetworkCase.Data;
using AlumniNetworkCase.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;

namespace AlumniNetworkCase.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class GroupController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        private readonly UserManager<ApplicationUser> _userManager;

        public GroupController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
            _context = context;
        }

        // GET: api/Group
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Group>>> GetGroups()
        {
            return await _context.Groups.Include("GroupMembers.Profile").ToListAsync();
        }

        // GET: api/Group/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Group>> GetGroup(int id)
        {
            var @group = await _context.Groups.Where(g => g.Id == id).Include(g => g.GroupMembers).ThenInclude(gm => gm.Profile).Include(g => g.Posts).ThenInclude(p => p.Sender).FirstOrDefaultAsync();

            if (@group == null)
            {
                return NotFound();
            }

            return @group;
        }

        // PUT: api/Group/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutGroup(int id, Group @group)
        {
            if (id != @group.Id)
            {
                return BadRequest();
            }

            _context.Entry(@group).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GroupExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Group
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Group>> PostGroup(Group group)
        {
            _context.Groups.Add(group);
            await _context.SaveChangesAsync();
            string userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            _context.GroupMembers.Add(new GroupMember(group.Id, userId));
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetGroup", new { id = group.Id }, group);
        }

        // DELETE: api/Group/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Group>> DeleteGroup(int id)
        {
            var @group = await _context.Groups.FindAsync(id);
            if (@group == null)
            {
                return NotFound();
            }

            _context.Groups.Remove(@group);
            await _context.SaveChangesAsync();

            return @group;
        }

        // POST: api/Group/{id}/join
        [HttpPost("{groupId}/join")]
        public async Task<ActionResult<Group>> JoinGroup(int groupId, string[] @newMembers)
        {
            var group = await _context.Groups.FindAsync(groupId);
            string currentUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            bool userIsMember = await _context.GroupMembers.FindAsync(groupId, currentUserId) != null;
            if (group == null)
            {
                return NotFound();
            }

            if (@newMembers.Length == 0)
            {
                if (group.IsPrivate)
                {
                    return Forbid();
                } else if (userIsMember)
                {
                    return Conflict("User is already a member of this group.");
                }
                _context.GroupMembers.Add(new GroupMember(groupId, currentUserId));
            } else
            {
                if (group.IsPrivate && !userIsMember)
                {
                    return Forbid("Only existing members can add new members");
                }
                for (int i = 0; i < @newMembers.Length; i++)
                {
                    string newMemberId = @newMembers[i];
                    var @groupMember = await _context.GroupMembers.FindAsync(groupId, newMemberId);
                    if (@groupMember != null)
                    {
                        return Conflict("User is already a member of this group.");
                    }
                    _context.GroupMembers.Add(new GroupMember(groupId, newMemberId));
                }
            }

            await _context.SaveChangesAsync();

            return await _context.Groups.Where(g => g.Id == groupId).Include(g => g.Posts).Include(g => g.GroupMembers).FirstOrDefaultAsync();
        }


        // DELETE: api/{id}/leave
        [HttpDelete("{groupId}/leave")]
        public async Task<ActionResult<Group>> LeaveGroup(int groupId)
        {
            var group = await _context.Groups.FindAsync(groupId);
            string currentUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            if (group == null || currentUserId == null)
            {
                return NotFound();
            }

            var groupMember = await _context.GroupMembers.FindAsync(groupId, currentUserId);
            if (groupMember == null)
            {
                return Conflict("User is not a member of this group.");
            }

            _context.GroupMembers.Remove(groupMember);
            await _context.SaveChangesAsync();

            return await _context.Groups.Where(g => g.Id == groupId).Include(g => g.Posts).Include(g => g.GroupMembers).FirstOrDefaultAsync();
        }

        private bool GroupExists(int id)
        {
            return _context.Groups.Any(e => e.Id == id);
        }
    }
}
