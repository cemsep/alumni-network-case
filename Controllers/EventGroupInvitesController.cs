﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AlumniNetworkCase.Data;
using AlumniNetworkCase.Models;
using Microsoft.AspNetCore.Authorization;

namespace AlumniNetworkCase.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class EventGroupInvitesController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public EventGroupInvitesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/EventGroupInvites
        [HttpGet]
        public async Task<ActionResult<IEnumerable<EventGroupInvite>>> GetEventGroupInvites()
        {
            return await _context.EventGroupInvites.ToListAsync();
        }

        // GET: api/EventGroupInvites/5
        [HttpGet("{id}")]
        public async Task<ActionResult<EventGroupInvite>> GetEventGroupInvite(int id)
        {
            var eventGroupInvite = await _context.EventGroupInvites.FindAsync(id);

            if (eventGroupInvite == null)
            {
                return NotFound();
            }

            return eventGroupInvite;
        }

        // PUT: api/EventGroupInvites/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutEventGroupInvite(int id, EventGroupInvite eventGroupInvite)
        {
            if (id != eventGroupInvite.Id)
            {
                return BadRequest();
            }

            _context.Entry(eventGroupInvite).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EventGroupInviteExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/EventGroupInvites
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<EventGroupInvite>> PostEventGroupInvite(EventGroupInvite eventGroupInvite)
        {
            _context.EventGroupInvites.Add(eventGroupInvite);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetEventGroupInvite", new { id = eventGroupInvite.Id }, eventGroupInvite);
        }

        // DELETE: api/EventGroupInvites/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<EventGroupInvite>> DeleteEventGroupInvite(int id)
        {
            var eventGroupInvite = await _context.EventGroupInvites.FindAsync(id);
            if (eventGroupInvite == null)
            {
                return NotFound();
            }

            _context.EventGroupInvites.Remove(eventGroupInvite);
            await _context.SaveChangesAsync();

            return eventGroupInvite;
        }

        private bool EventGroupInviteExists(int id)
        {
            return _context.EventGroupInvites.Any(e => e.Id == id);
        }
    }
}
