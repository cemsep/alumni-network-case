﻿using AlumniNetworkCase.Models;
using IdentityServer4.EntityFramework.Options;
using Microsoft.AspNetCore.ApiAuthorization.IdentityServer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlumniNetworkCase.Data
{
    public class ApplicationDbContext : ApiAuthorizationDbContext<ApplicationUser>
    {
        public ApplicationDbContext(
            DbContextOptions options,
            IOptions<OperationalStoreOptions> operationalStoreOptions) : base(options, operationalStoreOptions)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<GroupMember>()
                .HasKey(gm => new { gm.GroupId, gm.ProfileId });

            modelBuilder.Entity<TopicMember>()
                .HasKey(tm => new { tm.TopicId, tm.ProfileId });

            modelBuilder.Entity<EventMember>()
                .HasKey(em => new { em.EventId, em.ProfileId });
        }

        public DbSet<Profile> Profiles { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<EventMember> EventMembers { get; set; }
        public DbSet<EventGroupInvite> EventGroupInvites { get; set; }
        public DbSet<EventTopicInvite> EventTopicInvites { get; set; }
        public DbSet<EventUserInvite> EventUserInvites { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<GroupMember> GroupMembers { get; set; }
        public DbSet<RSVP> RSVPs { get; set; }
        public DbSet<Topic> Topics { get; set; }
        public DbSet<TopicMember> TopicMembers { get; set; }
    }
}
